export type DamHttpHeaders = HeadersInit;
export interface DamHttpRequestOptions {
    url: string;
    method?: string;
    headers?: DamHttpHeaders;
    noCache?: boolean;
    noCors?: boolean;
    payloadAsParams?: boolean;
    asText?: boolean;
}
export type DamHttpClientRequest = DamHttpRequestOptions & RequestInit;
export type DamHttpClientResponse = Response;
export interface DamHttpClientErrorResponse {
    status?: number;
    response?: DamHttpClientResponse;
    request?: DamHttpClientRequest;
    error?: unknown;
}
export type DamHttpClientRequestHookHandler = (request: DamHttpClientRequest) => Promise<DamHttpClientRequest>;
export type DamHttpClientResponseHookHandler = (request: DamHttpClientRequest, response: DamHttpClientResponse) => Promise<DamHttpClientResponse>;
export interface DamHttpClientHook<T> {
    id: number;
    handler: T;
}
export declare class DamHttpClient {
    private static _addHook;
    private static _removeHook;
    static addRequestHook(handler: DamHttpClientRequestHookHandler): number;
    static removeRequestHook(handlerId: number): void;
    static addResponseHook(handler: DamHttpClientResponseHookHandler): number;
    static removeResponseHook(handlerId: number): void;
    static addAuthRefreshTokenHook(refreshTokenFunc: (request: DamHttpClientRequest, response: DamHttpClientResponse) => Promise<string>): number;
    protected _action<ReqType, RespType>(opts: DamHttpRequestOptions, payload?: ReqType): Promise<RespType | string>;
    get<RespType>(opts: DamHttpRequestOptions | string): Promise<RespType | string>;
    post<ReqType, RespType>(opts: DamHttpRequestOptions | string, payload: ReqType): Promise<RespType | string>;
}
//# sourceMappingURL=DamHttpClient.d.ts.map