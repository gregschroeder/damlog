"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamlogBufferStream = void 0;
var Damlog_1 = require("./Damlog");
var ENTRY_LIMIT = 500;
var DamlogBufferStream = /** @class */ (function () {
    function DamlogBufferStream(buffer) {
        this._buffer = buffer;
    }
    DamlogBufferStream.prototype.write = function (rec) {
        var _a, _b;
        var logError = function (err) {
            /* istanbul ignore next */
            console.error('error attempting to log buffer stream', err);
        };
        // aim to never throw an error from a logging system, so put whole thing in a try/catch
        try {
            // get file path from environment for this name
            var entryLimit = ENTRY_LIMIT;
            // look in a name specific see if we find a level-specific setting or default,
            var settings = (_b = (_a = Damlog_1.Damlog.configSettings(rec.name)) === null || _a === void 0 ? void 0 : _a.buffer) === null || _b === void 0 ? void 0 : _b.settings;
            if (settings) {
                var levelName = Damlog_1.Damlog.stringFromLevel(rec.level);
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                var levelSettings = settings[levelName];
                entryLimit = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.entryLimit;
            }
            // failing that, look for the default
            if (!entryLimit) {
                if (settings) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    var levelSettings = settings['default'];
                    entryLimit = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.entryLimit;
                }
            }
            // add entry
            this.writeBuffer(rec);
            // prune buffer
            this.limitBuffer(entryLimit);
        }
        catch (err) {
            /* istanbul ignore next */
            logError(err);
            /* istanbul ignore next */
            return;
        }
    };
    DamlogBufferStream.prototype.writeBuffer = function (rec) {
        // add entry
        this._buffer.push(JSON.stringify(rec));
    };
    DamlogBufferStream.prototype.limitBuffer = function (entryLimit) {
        while (this._buffer.length > (entryLimit || ENTRY_LIMIT)) {
            this._buffer.shift();
        }
    };
    return DamlogBufferStream;
}());
exports.DamlogBufferStream = DamlogBufferStream;
