"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamHttpClient = void 0;
var requestHooks = [];
var responseHooks = [];
var DamHttpClient = /** @class */ (function () {
    function DamHttpClient() {
    }
    DamHttpClient._addHook = function (type, handler) {
        var hooks = type === 'request' ? requestHooks : responseHooks;
        var id = hooks.length ? hooks.sort(function (a, b) { return a.id - b.id; })[0].id + 1 : 1;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        hooks.push({ id: id, handler: handler });
        return id;
    };
    DamHttpClient._removeHook = function (type, handlerId) {
        var hooks = type === 'request' ? requestHooks : responseHooks;
        var index = hooks.findIndex(function (hook) { return hook.id === handlerId; });
        if (index >= 0) {
            hooks.splice(index, 1);
        }
    };
    DamHttpClient.addRequestHook = function (handler) {
        return DamHttpClient._addHook('request', handler);
    };
    DamHttpClient.removeRequestHook = function (handlerId) {
        DamHttpClient._removeHook('request', handlerId);
    };
    DamHttpClient.addResponseHook = function (handler) {
        return DamHttpClient._addHook('response', handler);
    };
    DamHttpClient.removeResponseHook = function (handlerId) {
        DamHttpClient._removeHook('response', handlerId);
    };
    DamHttpClient.addAuthRefreshTokenHook = function (refreshTokenFunc) {
        var _this = this;
        var authRefreshTokenHandler = function (request, response) { return __awaiter(_this, void 0, void 0, function () {
            var token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!((response === null || response === void 0 ? void 0 : response.status) === 401)) return [3 /*break*/, 2];
                        return [4 /*yield*/, refreshTokenFunc(request, response)];
                    case 1:
                        token = _a.sent();
                        request.headers = __assign(__assign({}, request.headers), { 'Authorization': "Bearer ".concat(token) });
                        // call fetch again, directly, and not via our wrapper,
                        // so no repeat of hooks and no recursion
                        return [2 /*return*/, fetch(request.url, request)];
                    case 2: 
                    // just pass through in other cases
                    return [2 /*return*/, Promise.resolve(response)];
                }
            });
        }); };
        return DamHttpClient.addResponseHook(authRefreshTokenHandler);
    };
    DamHttpClient.prototype._action = function (opts, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var noCacheHeaders, actionOpts, request, params, _i, _a, hook, error_1, errorResponse, response, _b, _c, hook, error_2, errorResponse, errorResponse, body, data;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        noCacheHeaders = opts.noCache ? {
                            'Pragma': 'no-cache',
                            'Cache-Control': 'no-cache',
                        } : {};
                        actionOpts = __assign({}, opts);
                        actionOpts.headers = __assign(__assign({
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        }, noCacheHeaders), opts.headers || {});
                        request = actionOpts;
                        if (opts.noCors) {
                            request.mode = 'no-cors';
                        }
                        if (opts.method === 'POST' && payload) {
                            if (opts.payloadAsParams) {
                                params = new URLSearchParams(payload);
                                request.body = params.toString();
                            }
                            else {
                                request.body = JSON.stringify(payload);
                            }
                        }
                        _i = 0, _a = requestHooks.reverse();
                        _d.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 6];
                        hook = _a[_i];
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, hook.handler(request)];
                    case 3:
                        request = _d.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _d.sent();
                        errorResponse = {
                            request: request,
                            error: error_1,
                        };
                        return [2 /*return*/, Promise.reject(errorResponse)];
                    case 5:
                        _i++;
                        return [3 /*break*/, 1];
                    case 6: return [4 /*yield*/, fetch(request.url, request)
                            .catch(function (error) {
                            var errorResponse = {
                                request: request,
                                error: error,
                            };
                            return Promise.reject(errorResponse);
                        })];
                    case 7:
                        response = _d.sent();
                        _b = 0, _c = responseHooks.reverse();
                        _d.label = 8;
                    case 8:
                        if (!(_b < _c.length)) return [3 /*break*/, 13];
                        hook = _c[_b];
                        _d.label = 9;
                    case 9:
                        _d.trys.push([9, 11, , 12]);
                        return [4 /*yield*/, hook.handler(request, response)];
                    case 10:
                        response = _d.sent();
                        return [3 /*break*/, 12];
                    case 11:
                        error_2 = _d.sent();
                        errorResponse = {
                            status: response.status,
                            request: request,
                            response: response,
                            error: error_2,
                        };
                        return [2 /*return*/, Promise.reject(errorResponse)];
                    case 12:
                        _b++;
                        return [3 /*break*/, 8];
                    case 13:
                        // treat http error status codes as client errors
                        if (response.status >= 400) {
                            errorResponse = {
                                status: response.status,
                                request: request,
                                response: response,
                            };
                            return [2 /*return*/, Promise.reject(errorResponse)];
                        }
                        if (!opts.asText) return [3 /*break*/, 15];
                        return [4 /*yield*/, response.text()
                                .catch(function (error) {
                                var errorResponse = {
                                    status: response.status,
                                    request: request,
                                    response: response,
                                    error: error,
                                };
                                return Promise.reject(errorResponse);
                            })];
                    case 14:
                        body = _d.sent();
                        return [2 /*return*/, Promise.resolve(body)];
                    case 15:
                        data = response.json()
                            .catch(function (error) {
                            var errorResponse = {
                                status: response.status,
                                request: request,
                                response: response,
                                error: error,
                            };
                            return Promise.reject(errorResponse);
                        });
                        return [2 /*return*/, Promise.resolve(data)];
                }
            });
        });
    };
    DamHttpClient.prototype.get = function (opts) {
        var options;
        if (typeof opts === 'string') {
            options = { url: opts, method: 'GET' };
        }
        else {
            options = opts;
        }
        options.method = 'GET';
        return this._action(options);
    };
    DamHttpClient.prototype.post = function (opts, payload) {
        var options;
        if (typeof opts === 'string') {
            options = { url: opts, method: 'POST' };
        }
        else {
            options = opts;
        }
        options.method = 'POST';
        return this._action(options, payload);
    };
    return DamHttpClient;
}());
exports.DamHttpClient = DamHttpClient;
