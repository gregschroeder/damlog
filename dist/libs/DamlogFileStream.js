"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamlogFileStream = void 0;
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
var fs_1 = __importDefault(require("fs"));
var Damlog_1 = require("./Damlog");
// obviously not supported in browsers
// webpack will thunk out fs
var DamlogFileStream = /** @class */ (function () {
    function DamlogFileStream() {
    }
    DamlogFileStream.prototype.write = function (rec) {
        var _a, _b;
        var logError = function (err) {
            console.error('error attempting to log filestream', err);
        };
        // aim to never throw an error from a logging system, so put whole thing in a try/catch
        try {
            // get file path from environment for this name
            var filePath = null;
            // look in a name specific see if we find a level-specific setting or default,
            var settings = (_b = (_a = Damlog_1.Damlog.configSettings(rec.name)) === null || _a === void 0 ? void 0 : _a.file) === null || _b === void 0 ? void 0 : _b.settings;
            if (settings) {
                var levelName = Damlog_1.Damlog.stringFromLevel(rec.level);
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                var levelSettings = settings[levelName];
                filePath = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.filePath;
            }
            // failing that, look for the default
            if (!filePath) {
                if (settings) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    var levelSettings = settings['default'];
                    filePath = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.filePath;
                }
            }
            if (!filePath) {
                logError(new Error('unable to find filePath in settings'));
                return;
            }
            // convert output to string
            var output = JSON.stringify(rec);
            if (output) {
                fs_1.default.appendFileSync(filePath, output);
            }
        }
        catch (err) {
            logError(err);
            return;
        }
    };
    return DamlogFileStream;
}());
exports.DamlogFileStream = DamlogFileStream;
