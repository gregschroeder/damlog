"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
var fs_1 = __importDefault(require("fs"));
var simple_crypto_js_1 = require("simple-crypto-js");
var DamHttpClient_1 = require("./DamHttpClient");
var Config = /** @class */ (function () {
    function Config(noInit) {
        if (noInit === void 0) { noInit = false; }
        this._env = {};
        if (Config._instance) {
            return Config._instance;
        }
        Config._instance = this;
        if (!noInit) {
            Config.initConfig();
        }
    }
    Config.initConfig = function (overrides, overwrite) {
        if (overrides === void 0) { overrides = {}; }
        if (overwrite === void 0) { overwrite = true; }
        return __awaiter(this, void 0, void 0, function () {
            var me, uri, err_1, node_env, DAMLOG_ENV;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        me = Config._instance || new Config(true);
                        // copy any process.env values
                        if (typeof process !== 'undefined' && process.env) {
                            Object.assign(me._env, process.env);
                        }
                        // add overrides, if any
                        Object.assign(me._env, overrides);
                        uri = me._env.DAMLOG_ENV_URI;
                        if (!uri) return [3 /*break*/, 4];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, me.loadURIEnvConfig(uri, overwrite)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log("error loading config from ".concat(uri), err_1);
                        throw err_1;
                    case 4:
                        node_env = me._env.NODE_ENV;
                        if (!me._env.DAMLOG_ENV) {
                            if (node_env) {
                                DAMLOG_ENV = 'dev';
                                if (node_env.startsWith('prod')) {
                                    DAMLOG_ENV = 'prod';
                                }
                                else if (node_env === 'test') {
                                    DAMLOG_ENV = 'test';
                                }
                                me._env.DAMLOG_ENV = DAMLOG_ENV;
                            }
                            else {
                                me._env.DAMLOG_ENV = 'dev';
                            }
                        }
                        Config._instance = me;
                        return [2 /*return*/, me];
                }
            });
        });
    };
    Object.defineProperty(Config.prototype, "env", {
        get: function () {
            return this._env;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Config, "instance", {
        get: function () {
            return new Config();
        },
        enumerable: false,
        configurable: true
    });
    Config._resetInstance = function () {
        // for unit testing purposes, reset the singleton
        if (Config._instance) {
            Object.assign(Config._instance._env, {});
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            Config._instance = undefined;
        }
    };
    Object.defineProperty(Config, "env", {
        get: function () {
            return Config.instance._env;
        },
        enumerable: false,
        configurable: true
    });
    Config.envStage = function () {
        // return configured stage, defaults to dev
        var DAMLOG_ENV = Config.env.DAMLOG_ENV;
        return DAMLOG_ENV;
    };
    // noinspection JSUnusedGlobalSymbols
    Config.isProduction = function () {
        return Config.envStage() === 'prod';
    };
    // noinspection JSUnusedGlobalSymbols
    Config.isDev = function () {
        return Config.envStage() === 'dev';
    };
    // noinspection JSUnusedGlobalSymbols
    Config.messages = function () {
        return {
            extConfig: 'Copyright - All Rights Reserved',
            auth: 'Authentication: Basic',
            gatherTemplates: 'IGatherTemplates for parsing',
            grabber: 'Grabber IGrabberTemplate IMetaData',
            parser: 'Profile and other data parser',
        };
    };
    Config.parserDataOpts = function (flags) {
        return {
            reduce: (flags === null || flags === void 0 ? void 0 : flags.indexOf('r')) >= 0,
            encode: (flags === null || flags === void 0 ? void 0 : flags.indexOf('e')) >= 0,
            compress: (flags === null || flags === void 0 ? void 0 : flags.indexOf('c')) >= 0,
        };
    };
    Config.parserDataOptsStr = function (opts) {
        return "".concat(opts.reduce ? 'r' : '').concat(opts.encode ? 'e' : '').concat(opts.compress ? 'c' : '');
    };
    // noinspection JSUnusedGlobalSymbols
    Config.forSend = function (message, payload) {
        var encr = new simple_crypto_js_1.SimpleCrypto(message);
        return encr.encrypt(payload);
    };
    // noinspection JSUnusedGlobalSymbols
    Config.forReceipt = function (message, payload) {
        var encr = new simple_crypto_js_1.SimpleCrypto(message);
        return encr.decrypt(payload);
    };
    // load config from URI
    Config.prototype.loadURIEnvConfig = function (uri, overwrite) {
        if (overwrite === void 0) { overwrite = true; }
        return __awaiter(this, void 0, void 0, function () {
            var httpClient, body, bodyObj, path, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!uri.startsWith('http')) return [3 /*break*/, 2];
                        httpClient = new DamHttpClient_1.DamHttpClient();
                        return [4 /*yield*/, httpClient.get({ url: uri, noCache: true, asText: true })];
                    case 1:
                        body = _a.sent();
                        if (body) {
                            bodyObj = Config.forReceipt(Config.messages().extConfig, body);
                            if (bodyObj) {
                                this.loadEnvConfig(bodyObj, overwrite);
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        if (uri.startsWith('file://') || uri.startsWith('/') || uri.startsWith('./') || uri.startsWith('../')) {
                            path = uri.replace(/^file:\/+/, '').replace(/\?.*/, '');
                            if (fs_1.default.existsSync(path)) {
                                data = fs_1.default.readFileSync(path, { encoding: 'utf-8' });
                                this.loadJSONEnvConfig(data, overwrite);
                            }
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // load JSON config into config env
    Config.prototype.loadJSONEnvConfig = function (jsonString, overwrite) {
        if (overwrite === void 0) { overwrite = true; }
        this.loadEnvConfig(JSON.parse(jsonString), overwrite);
    };
    // load env object config into config env
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Config.prototype.loadEnvConfig = function (envConfig, overwrite) {
        if (overwrite === void 0) { overwrite = true; }
        for (var key in envConfig) {
            // if value exists, merge in the new values, otherwise add it
            if (this._env[key]) {
                if (overwrite) {
                    this._env[key] = Config.mergeDeep(this._env[key], envConfig[key]);
                }
                else {
                    this._env[key] = Config.mergeDeep(envConfig[key], this._env[key]);
                }
            }
            else {
                this._env[key] = envConfig[key];
            }
        }
    };
    // replace env object config
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Config.prototype.setEnvConfig = function (envConfig) {
        this._env = envConfig;
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Config.isObject = function (item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Config.mergeDeep = function (target) {
        var _a, _b;
        var sources = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            sources[_i - 1] = arguments[_i];
        }
        if (!sources.length) {
            return target;
        }
        var source = sources.shift();
        if (Config.isObject(target) && this.isObject(source)) {
            for (var key in source) {
                if (this.isObject(source[key])) {
                    if (!target[key]) {
                        Object.assign(target, (_a = {}, _a[key] = {}, _a));
                    }
                    Config.mergeDeep(target[key], source[key]);
                }
                else {
                    Object.assign(target, (_b = {}, _b[key] = source[key], _b));
                }
            }
        }
        return Config.mergeDeep.apply(Config, __spreadArray([target], sources, false));
    };
    return Config;
}());
exports.default = Config;
