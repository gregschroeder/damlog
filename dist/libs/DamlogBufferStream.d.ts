import { DamlogRecord, DamlogStream } from '../types/Damlog';
export declare class DamlogBufferStream implements DamlogStream {
    protected _buffer: string[];
    constructor(buffer: string[]);
    write(rec: DamlogRecord): void;
    protected writeBuffer(rec: DamlogRecord): void;
    protected limitBuffer(entryLimit: number): void;
}
//# sourceMappingURL=DamlogBufferStream.d.ts.map