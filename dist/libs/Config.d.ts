import { EnvStage } from '../types/EnvStage';
import { ParserPayloadOptions } from '../types/Config';
interface EnvType {
    [key: string]: any;
}
export default class Config {
    private static _instance;
    private _env;
    constructor(noInit?: boolean);
    static initConfig(overrides?: {}, overwrite?: boolean): Promise<Config>;
    get env(): EnvType;
    static get instance(): Config;
    static _resetInstance(): void;
    static get env(): EnvType;
    static envStage(): EnvStage;
    static isProduction(): boolean;
    static isDev(): boolean;
    static messages(): {
        [key: string]: string;
    };
    static parserDataOpts(flags: string): ParserPayloadOptions;
    static parserDataOptsStr(opts: ParserPayloadOptions): string;
    static forSend(message: string, payload: any): string;
    static forReceipt(message: string, payload: string): any;
    loadURIEnvConfig(uri: string, overwrite?: boolean): Promise<void>;
    loadJSONEnvConfig(jsonString: string, overwrite?: boolean): void;
    loadEnvConfig(envConfig: any, overwrite?: boolean): void;
    setEnvConfig(envConfig: any): void;
    static isObject(item: any): boolean;
    static mergeDeep(target: any, ...sources: any): any;
}
export {};
//# sourceMappingURL=Config.d.ts.map