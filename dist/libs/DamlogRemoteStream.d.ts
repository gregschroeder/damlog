import { DamlogRecord, DamlogStream } from '../types/Damlog';
export declare class DamlogRemoteStream implements DamlogStream {
    write(rec: DamlogRecord): void;
}
//# sourceMappingURL=DamlogRemoteStream.d.ts.map