import { DamlogRecord, DamlogStream } from '../types/Damlog';
export declare class DamlogFileStream implements DamlogStream {
    write(rec: DamlogRecord): void;
}
//# sourceMappingURL=DamlogFileStream.d.ts.map