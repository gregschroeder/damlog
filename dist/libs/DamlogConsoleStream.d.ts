import { DamlogDarkMode, DamlogFormatting, DamlogRecord, DamlogStream } from '../types/Damlog';
export declare const DAMLOG_DEFAULT_FORMATTING: {
    [key in DamlogDarkMode]: DamlogFormatting;
};
export declare class DamlogConsoleStream implements DamlogStream {
    private _inBrowser;
    private static padZeroes;
    private formatItemChalk;
    private formatItemCSS;
    private formatItem;
    write(rec: DamlogRecord): void;
}
//# sourceMappingURL=DamlogConsoleStream.d.ts.map