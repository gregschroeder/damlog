"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamlogRemoteStream = void 0;
var Damlog_1 = require("./Damlog");
var DamHttpClient_1 = require("./DamHttpClient");
var DamlogRemoteStream = /** @class */ (function () {
    function DamlogRemoteStream() {
    }
    DamlogRemoteStream.prototype.write = function (rec) {
        var _a, _b;
        var logError = function (err) {
            console.error('error attempting to log remote stream', err);
        };
        // aim to never throw an error from a logging system, so put whole thing in a try/catch
        try {
            // get loggerUrl and optional filePath from environment for this name
            var loggerUrl = null;
            var filePath = null;
            // look in a name specific see if we find a level-specific setting or default,
            var settings = (_b = (_a = Damlog_1.Damlog.configSettings(rec.name)) === null || _a === void 0 ? void 0 : _a.remote) === null || _b === void 0 ? void 0 : _b.settings;
            if (settings) {
                var levelName = Damlog_1.Damlog.stringFromLevel(rec.level);
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                var levelSettings = settings[levelName];
                loggerUrl = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.loggerUrl;
                filePath = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.filePath;
            }
            // failing that, look for the default
            if (!loggerUrl) {
                filePath = null;
                if (settings) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    var levelSettings = settings['default'];
                    loggerUrl = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.loggerUrl;
                    filePath = levelSettings === null || levelSettings === void 0 ? void 0 : levelSettings.filePath;
                }
            }
            if (!loggerUrl) {
                logError(new Error('unable to find loggerUrl in settings'));
                return;
            }
            // get any unknown args, relay separately in dataStr
            var logArgs = [];
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            for (var _i = 0, _c = Object.entries(rec); _i < _c.length; _i++) {
                var _d = _c[_i], key = _d[0], value = _d[1];
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                if (!['name', 'level', 'levelName', 'msg', 'time', 'v'].includes(key)) {
                    var obj = {};
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    obj[key] = value;
                    logArgs.push(obj);
                }
            }
            var body = {
                name: rec.name,
                level: Damlog_1.Damlog.stringFromLevel(rec.level),
                msg: rec.msg,
            };
            if (logArgs.length) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                body.dataStr = logArgs.length === 1 ? JSON.stringify(logArgs[0]) : JSON.stringify(logArgs);
            }
            if (filePath) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                body.filePath = filePath;
            }
            var httpClient = new DamHttpClient_1.DamHttpClient();
            // fetch(loggerUrl, {
            //   method: 'POST',
            //   headers: {
            //     'Accept': 'application/json',
            //     'Content-Type': 'application/json',
            //     'Pragma': 'no-cache',
            //     'Cache-Control': 'no-cache',
            //     // TODO: where to get auth headers?
            //   },
            //   body: JSON.stringify(body),
            // }).then(response => response.json())
            httpClient.post({ url: loggerUrl, noCache: true }, JSON.stringify(body))
                .then(function (response) {
                var _a;
                var body = response;
                /* istanbul ignore next */
                if (!((_a = body === null || body === void 0 ? void 0 : body.message) === null || _a === void 0 ? void 0 : _a.includes("logged ".concat(rec.name)))) {
                    if (body === null || body === void 0 ? void 0 : body.message) {
                        throw new Error(body.message);
                    }
                    else {
                        throw new Error('missing response message');
                    }
                }
            })
                .catch(function (err) {
                logError(err);
            });
        }
        catch (err) {
            /* istanbul ignore next */
            logError(err);
            /* istanbul ignore next */
            return;
        }
    };
    return DamlogRemoteStream;
}());
exports.DamlogRemoteStream = DamlogRemoteStream;
