"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Damlog = void 0;
var browser_bunyan_1 = require("browser-bunyan");
var Damlog_1 = require("../types/Damlog");
var DamlogConsoleStream_1 = require("./DamlogConsoleStream");
var Config_1 = __importDefault(require("./Config"));
var DamlogFileStream_1 = require("./DamlogFileStream");
var DamlogRemoteStream_1 = require("./DamlogRemoteStream");
var DamlogBufferStream_1 = require("./DamlogBufferStream");
// fallback settings if none are found in Config
// also serves as a somewhat easier documentation example than TypeScript type navigation
var DEFAULT_DAMLOG_SETTINGS = {
    default: {
        console: {
            level: Damlog_1.DamlogLevel.OFF,
            settings: {
                default: {
                    darkMode: 'dark',
                },
            },
        },
        remote: {
            level: Damlog_1.DamlogLevel.OFF,
            settings: {
                default: {
                    loggerUrl: 'http://localhost:3000/dev/logRelay',
                    filePath: '/tmp/damli-remote.log',
                },
                // other levels would go here, to say, put tracing in a different place
                // TRACE: { ... }
            }
        },
        file: {
            level: Damlog_1.DamlogLevel.OFF,
            settings: {
                default: {
                    filePath: '/tmp/damli.log',
                },
                // other levels would go here, to say, put tracing in a different place
                // TRACE: { ... }
            }
        },
        buffer: {
            level: Damlog_1.DamlogLevel.OFF,
            settings: {
                default: {
                    entryLimit: 500,
                },
            }
        },
    }
};
// global in-memory log buffer
var logBuffer = [];
var defaultDamlogOutputStreams = {
    console: new DamlogConsoleStream_1.DamlogConsoleStream(),
    remote: new DamlogRemoteStream_1.DamlogRemoteStream(),
    file: new DamlogFileStream_1.DamlogFileStream(),
    buffer: new DamlogBufferStream_1.DamlogBufferStream(logBuffer),
};
var Damlog = /** @class */ (function () {
    function Damlog(name, logger) {
        this._name = name;
        this._logger = logger;
    }
    Damlog.stringFromLevel = function (level) {
        return Damlog_1.DamlogLevel[level];
    };
    Damlog.levelFromString = function (val) {
        if (val === null) {
            throw new Error('Argument must be set');
        }
        switch (val.toLowerCase()) {
            case 'trace':
                return Damlog_1.DamlogLevel.TRACE;
            case 'debug':
                return Damlog_1.DamlogLevel.DEBUG;
            case 'info':
                return Damlog_1.DamlogLevel.INFO;
            case 'warn':
                return Damlog_1.DamlogLevel.WARN;
            case 'error':
                return Damlog_1.DamlogLevel.ERROR;
            case 'fatal':
                return Damlog_1.DamlogLevel.FATAL;
            case 'off':
                return Damlog_1.DamlogLevel.OFF;
            default:
                throw new Error('Unsupported value for conversion: ' + val);
        }
    };
    ;
    Damlog.createLogger = function (name, overrideOutputStreams) {
        // determine output streams
        var outputStreams = Object.assign({}, defaultDamlogOutputStreams, overrideOutputStreams || {});
        // create stream dispatcher for this name
        var streamDispatcher = Damlog.streamDispatcher(name, outputStreams);
        // create the underlying Bunyan logger.
        // set to trace so at its level it logs everything,
        // and the actual level check happening in the dispatcher
        var bunyanLogger = (0, browser_bunyan_1.createLogger)({ name: name, stream: streamDispatcher, level: browser_bunyan_1.TRACE, src: false });
        // instantiate and return our wrapper
        return new Damlog(name, bunyanLogger);
    };
    // create a stream dispatcher that has all available output streams,
    // and looks up the config to determine which one(s) to delegate the writes to
    Damlog.streamDispatcher = function (name, outputStreams) {
        var dispatcher = {
            write: function (record) {
                var settings = Damlog.configSettings(name);
                if (settings) {
                    for (var _i = 0, _a = ['console', 'remote', 'file', 'buffer']; _i < _a.length; _i++) {
                        var streamType = _a[_i];
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        if (settings[streamType] && record.level >= settings[streamType].level && outputStreams[streamType]) {
                            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                            // @ts-ignore
                            outputStreams[streamType].write(record);
                        }
                    }
                }
            }
        };
        return dispatcher;
    };
    // return the settings for the named logger, applying the inheritance hierarchy
    // - named values < 'default' values
    // - config env < default builtin
    Damlog.configSettings = function (name) {
        var _a, _b, _c, _d, _e, _f;
        var defaultSettings = ((_c = (_b = (_a = Config_1.default.env) === null || _a === void 0 ? void 0 : _a.logging) === null || _b === void 0 ? void 0 : _b.settings) === null || _c === void 0 ? void 0 : _c.default) || {};
        var namedSettings = ((_f = (_e = (_d = Config_1.default.env) === null || _d === void 0 ? void 0 : _d.logging) === null || _e === void 0 ? void 0 : _e.settings) === null || _f === void 0 ? void 0 : _f[name]) || {};
        // merge results in precedence order
        var result = Object.assign(DEFAULT_DAMLOG_SETTINGS.default, defaultSettings, namedSettings);
        return result || {};
    };
    Object.defineProperty(Damlog.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Damlog.prototype, "level", {
        get: function () {
            return this._logger.level();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Damlog.prototype, "buffer", {
        get: function () {
            // shared global buffer
            return logBuffer;
        },
        enumerable: false,
        configurable: true
    });
    Damlog.prototype.reset = function () {
        logBuffer.length = 0;
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype._log = function (method, msg, data) {
        if (typeof msg === 'string' && typeof data === 'object') {
            // Bunyan has a bizarre rule that has the object first and then the string,
            // in order to let the console do its object display, which is preferable
            // to just appending a stringified value.
            // so we intercept those here and swap them back for better functionality.
            // nest data in object to avoid collisions on the top level of objects,
            // where the log record has common properties like "name"
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            this._logger[method]({ data: data }, msg);
        }
        else if (typeof data !== 'undefined') {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            this._logger[method](msg, { data: data });
        }
        else {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            this._logger[method](msg);
        }
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.trace = function (msg, data) {
        this._log('trace', msg, data);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.debug = function (msg, data) {
        this._log('debug', msg, data);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.warn = function (msg, data) {
        this._log('warn', msg, data);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.info = function (msg, data) {
        this._log('info', msg, data);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.error = function (msg, data) {
        this._log('error', msg, data);
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Damlog.prototype.fatal = function (msg, data) {
        this._log('fatal', msg, data);
    };
    Damlog.prototype.alert = function (msg) {
        this._log('error', "ALERT: ".concat(msg));
        var theWindow = this._window();
        if (typeof theWindow !== 'undefined' && typeof theWindow.alert === 'function') {
            theWindow.alert(msg);
        }
    };
    // separate so mockable for testing
    Damlog.prototype._window = function () {
        return typeof window !== 'undefined' ? window : undefined;
    };
    return Damlog;
}());
exports.Damlog = Damlog;
