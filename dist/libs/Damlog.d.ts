import { DamlogLevel, DamlogLogger, DamlogSettings, DamlogStream, DamlogStreamType } from '../types/Damlog';
type OutputStream = {
    [key in DamlogStreamType]?: DamlogStream;
};
export declare class Damlog implements DamlogLogger {
    private readonly _name;
    private readonly _logger;
    static stringFromLevel(level: DamlogLevel): string;
    static levelFromString(val: string): DamlogLevel;
    static createLogger(name: string, overrideOutputStreams?: OutputStream): DamlogLogger;
    static streamDispatcher(name: string, outputStreams: OutputStream): DamlogStream;
    static configSettings(name: string): DamlogSettings;
    private constructor();
    get name(): string;
    get level(): DamlogLevel;
    get buffer(): string[];
    reset(): void;
    private _log;
    trace(msg: string | object, data?: any): void;
    debug(msg: string | object, data?: any): void;
    warn(msg: string | object, data?: any): void;
    info(msg: string | object, data?: any): void;
    error(msg: string | object, data?: any): void;
    fatal(msg: string | object, data?: any): void;
    alert(msg: string): void;
    _window(): Window | undefined;
}
export {};
//# sourceMappingURL=Damlog.d.ts.map