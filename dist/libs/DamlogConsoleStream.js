"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamlogConsoleStream = exports.DAMLOG_DEFAULT_FORMATTING = void 0;
var Damlog_1 = require("../types/Damlog");
var Damlog_2 = require("./Damlog");
var chalk_1 = __importDefault(require("chalk"));
// only consoles will support formatting
// logs are generally JSON and should not have control characters
exports.DAMLOG_DEFAULT_FORMATTING = {
    light: {
        default: {
            message: { color: Damlog_1.DamlogColor.black },
            timestamp: { color: Damlog_1.DamlogColor.gray, modifier: 'italic' },
            level: { color: Damlog_1.DamlogColor.cyan },
            name: { color: Damlog_1.DamlogColor.blue },
        },
        TRACE: {
            message: { color: Damlog_1.DamlogColor.magenta },
            level: { color: Damlog_1.DamlogColor.cyan, weight: 'bold' },
        },
        DEBUG: {
            message: { color: Damlog_1.DamlogColor.green },
            level: { color: Damlog_1.DamlogColor.green, weight: 'bold' },
        },
        INFO: {
            message: { color: Damlog_1.DamlogColor.black },
            level: { color: Damlog_1.DamlogColor.black },
            name: { color: Damlog_1.DamlogColor.black },
        },
        WARN: {
            message: { color: Damlog_1.DamlogColor.orange },
            level: { color: Damlog_1.DamlogColor.orange, weight: 'bold' },
        },
        ERROR: {
            message: { color: Damlog_1.DamlogColor.red },
            level: { color: Damlog_1.DamlogColor.red, weight: 'bold' },
        },
        FATAL: {
            message: { color: Damlog_1.DamlogColor.red },
            level: { color: Damlog_1.DamlogColor.yellow, backgroundColor: Damlog_1.DamlogColor.red, weight: 'bold' },
        },
    },
    dark: {
        default: {
            message: { color: Damlog_1.DamlogColor.white },
            timestamp: { color: Damlog_1.DamlogColor.gray, modifier: 'italic' },
            level: { color: Damlog_1.DamlogColor.cyan },
            name: { color: Damlog_1.DamlogColor.blue },
        },
        TRACE: {
            message: { color: Damlog_1.DamlogColor.magenta },
            level: { color: Damlog_1.DamlogColor.cyan, weight: 'bold' },
        },
        DEBUG: {
            message: { color: Damlog_1.DamlogColor.green },
            level: { color: Damlog_1.DamlogColor.green, weight: 'bold' },
        },
        INFO: {
            message: { color: Damlog_1.DamlogColor.white },
            level: { color: Damlog_1.DamlogColor.white },
            name: { color: Damlog_1.DamlogColor.white },
        },
        WARN: {
            message: { color: Damlog_1.DamlogColor.orange },
            level: { color: Damlog_1.DamlogColor.orange, weight: 'bold' },
        },
        ERROR: {
            message: { color: Damlog_1.DamlogColor.red },
            level: { color: Damlog_1.DamlogColor.red, weight: 'bold' },
        },
        FATAL: {
            message: { color: Damlog_1.DamlogColor.yellow },
            level: { color: Damlog_1.DamlogColor.yellow, backgroundColor: Damlog_1.DamlogColor.red, weight: 'bold' },
        },
    },
};
var DamlogConsoleStream = /** @class */ (function () {
    function DamlogConsoleStream() {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this._inBrowser = typeof process === 'undefined';
    }
    DamlogConsoleStream.padZeroes = function (number, len) {
        return Array((len + 1) - (number + '').length).join('0') + number;
    };
    DamlogConsoleStream.prototype.formatItemChalk = function (item, format, logDirective) {
        var result = item;
        // apply color
        if (format.color) {
            var color = "".concat(format.color);
            if (color.startsWith('#')) {
                result = chalk_1.default.hex(color)(result);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                result = chalk_1.default[color](result);
            }
        }
        // apply background color
        if (format.backgroundColor) {
            var backgroundColor = "".concat(format.backgroundColor);
            if (backgroundColor.startsWith('#')) {
                result = chalk_1.default.bgHex(backgroundColor)(result);
            }
            else {
                var bgColor = "bg".concat(backgroundColor.charAt(0).toUpperCase()).concat(backgroundColor.substring(1).toLowerCase());
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                result = chalk_1.default[bgColor](result);
            }
        }
        // apply weight
        if (format.weight === 'bold') {
            result = chalk_1.default.bold(result);
        }
        // apply modifier
        if (format.modifier === 'italic') {
            result = chalk_1.default.italic(result);
        }
        logDirective.push(result);
    };
    DamlogConsoleStream.prototype.formatItemCSS = function (item, format, logDirective, logArgs) {
        var cssArgs = [];
        // apply color
        if (format.color) {
            cssArgs.push("color: ".concat(format.color));
        }
        // apply background color
        if (format.backgroundColor) {
            cssArgs.push("background-color: ".concat(format.backgroundColor));
        }
        // apply weight
        if (format.weight === 'bold') {
            cssArgs.push('font-weight: bold');
        }
        // apply modifier
        if (format.modifier === 'italic') {
            cssArgs.push('font-style: italic');
        }
        if (cssArgs.length > 0) {
            // add %c to directive and args to arg list
            logDirective.push('%c');
            logArgs.push(cssArgs.join('; '));
            // add %s to directive and item string to arg list
            logDirective.push('%s');
            logArgs.push(item);
        }
        else {
            // no css, just push item inline in directive
            logDirective.push(item);
        }
    };
    DamlogConsoleStream.prototype.formatItem = function (item, format, logDirective, logArgs) {
        if (this._inBrowser) {
            return this.formatItemCSS(item, format, logDirective, logArgs);
        }
        return this.formatItemChalk(item, format, logDirective); // args not needed; all strings inline in directive
    };
    // format: [time] level: loggerName: msg
    DamlogConsoleStream.prototype.write = function (rec) {
        var _a, _b, _c;
        // timestamp
        var hours = DamlogConsoleStream.padZeroes(rec.time.getHours(), 2);
        var min = DamlogConsoleStream.padZeroes(rec.time.getMinutes(), 2);
        var sec = DamlogConsoleStream.padZeroes(rec.time.getSeconds(), 2);
        var ms = DamlogConsoleStream.padZeroes(rec.time.getMilliseconds(), 4);
        var time = "[".concat(hours, ":").concat(min, ":").concat(sec, ".").concat(ms, "]");
        // labels
        var levelName = Damlog_2.Damlog.stringFromLevel(rec.level);
        var loggerName = rec.childName ? rec.name + '/' + rec.childName : rec.name;
        // output function to use
        var consoleMethodLevel = levelName;
        if (rec.level === Damlog_1.DamlogLevel.DEBUG || rec.level === Damlog_1.DamlogLevel.TRACE) {
            // for tracing/debugging, in dev, use console.info() in browser, special code later to get 'details' with stacktrace
            // NB: process.env.DAMLOG_ENV substituted by webpack at build time
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            consoleMethodLevel = 'info';
        }
        else if (rec.level === Damlog_1.DamlogLevel.ERROR || rec.level === Damlog_1.DamlogLevel.FATAL) {
            // for errors, use console.error() in browser, but not in terminal
            consoleMethodLevel = this._inBrowser ? 'error' : 'log';
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var consoleMethod = typeof console[consoleMethodLevel] === 'function' ? console[consoleMethodLevel] : console.log;
        // apply level specific formatting
        var settings = Damlog_2.Damlog.configSettings(rec.name);
        var darkMode = ((_c = (_b = (_a = settings === null || settings === void 0 ? void 0 : settings.console) === null || _a === void 0 ? void 0 : _a.settings) === null || _b === void 0 ? void 0 : _b.default) === null || _c === void 0 ? void 0 : _c.darkMode) || 'dark';
        var baseFormatting = exports.DAMLOG_DEFAULT_FORMATTING[darkMode];
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var levelFormatting = baseFormatting[levelName];
        var formatting = Object.assign({}, baseFormatting.default, levelFormatting);
        // formulate directive string
        // [time] level: loggerName: msg obj
        var logDirective = [];
        var logArgs = []; // eslint-disable-line @typescript-eslint/no-explicit-any
        this.formatItem(time, formatting.timestamp, logDirective, logArgs);
        logDirective.push(' ');
        this.formatItem(levelName, formatting.level, logDirective, logArgs);
        logDirective.push(' ');
        this.formatItem(loggerName, formatting.name, logDirective, logArgs);
        logDirective.push(' ');
        this.formatItem('%s', formatting.message, logDirective, logArgs);
        // put logging command first, then any formatting args, then message and object arguments
        logArgs.unshift(logDirective.join(''));
        logArgs.push(rec.msg);
        // add any remaining unknown arguments as objects to log
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        for (var _i = 0, _d = Object.entries(rec); _i < _d.length; _i++) {
            var _e = _d[_i], key = _e[0], value = _e[1];
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            if (!['name', 'level', 'levelName', 'msg', 'time', 'v'].includes(key)) {
                var obj = {};
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                obj[key] = value;
                logArgs.push(obj);
            }
        }
        consoleMethod.apply(console, logArgs);
        // in browser, follow with collapsed group with trace() call to get sourcemapped stack track
        // will include logging subsystem but oh well...
        if (this._inBrowser) {
            console.groupCollapsed('');
            console.trace('stack');
            console.groupEnd();
        }
    };
    return DamlogConsoleStream;
}());
exports.DamlogConsoleStream = DamlogConsoleStream;
