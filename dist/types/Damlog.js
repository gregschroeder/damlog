"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DamlogLevel = exports.DamlogColor = void 0;
exports.DamlogColor = {
    black: 'black',
    gray: '#888888',
    red: '#FF033E',
    green: '#009A17',
    yellow: '#FEDD00',
    blue: '#2E67F8',
    magenta: '#D53F77',
    cyan: '#00BCE3',
    white: '#FFFFED',
    orange: '#FF8200'
};
// NB: keep values in sync with Bunyan for interoperability
var DamlogLevel;
(function (DamlogLevel) {
    DamlogLevel[DamlogLevel["TRACE"] = 10] = "TRACE";
    DamlogLevel[DamlogLevel["DEBUG"] = 20] = "DEBUG";
    DamlogLevel[DamlogLevel["INFO"] = 30] = "INFO";
    DamlogLevel[DamlogLevel["WARN"] = 40] = "WARN";
    DamlogLevel[DamlogLevel["ERROR"] = 50] = "ERROR";
    DamlogLevel[DamlogLevel["FATAL"] = 60] = "FATAL";
    DamlogLevel[DamlogLevel["OFF"] = 100] = "OFF";
})(DamlogLevel = exports.DamlogLevel || (exports.DamlogLevel = {}));
