import { IFieldTransform, ProfileType } from './Resources';
export type GathererSectionType = 'matcher' | 'page' | 'userInfo' | 'summary' | 'biography' | 'contactInfo' | 'positions' | 'education' | 'skills';
export type GathererAction = string;
export interface IGathererField {
    field: string;
    actions: GathererAction[];
}
export interface IGathererSection {
    sectionType: GathererSectionType;
    fields: IGathererField[];
}
export interface IGathererUrlMatchers {
    urlMatcherGlobs: string[];
    urlMatcherRegExps: string[];
}
export interface IGathererClientParser {
    urlMatcherGlobs: string[];
    urlMatcherRegExp: string;
    parserFieldTransforms: IFieldTransform[];
    reductionSelectors: string[];
}
export interface IGathererTemplate {
    site: string;
    type: ProfileType;
    version: string;
    active: boolean;
    clientParser: IGathererClientParser;
    gatherSections: IGathererSection[];
}
//# sourceMappingURL=Gatherer.d.ts.map