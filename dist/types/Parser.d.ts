import { IGathererTemplate } from './Gatherer';
import { IProfile } from './Profile';
export type ParsedItem = string | object | null | undefined;
export type FullParsedItem = ParsedItem | ParsedItem[];
export interface TypedItem {
    value: any;
    type: string;
}
export interface IParseOptsTypedValue {
    valueField?: string;
    typeField?: string;
    defaultType?: string;
    typeClassSelector?: string;
    valueClassSelector?: string;
    typeGetter?: (item: any) => string;
    valueGetter?: (item: any) => string;
    transformer?: (item: FullParsedItem) => FullParsedItem;
}
export interface IParseOpts {
    first?: boolean;
    attr?: string;
    pruneChildren?: boolean;
    typedValue?: IParseOptsTypedValue;
}
export interface IParserSelector {
    selector: string;
    opts?: IParseOpts;
}
export type ParserField = {
    [key: string]: IParserSelector[] | ParserField;
};
export interface IParserTemplate {
    parser: {
        parserName: string;
        fields: ParserField;
    };
}
export type ParserConfig = IGathererTemplate & IParserTemplate;
export type ParserResult = IProfile;
//# sourceMappingURL=Parser.d.ts.map