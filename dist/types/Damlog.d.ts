export declare const DamlogColor: {
    readonly black: "black";
    readonly gray: "#888888";
    readonly red: "#FF033E";
    readonly green: "#009A17";
    readonly yellow: "#FEDD00";
    readonly blue: "#2E67F8";
    readonly magenta: "#D53F77";
    readonly cyan: "#00BCE3";
    readonly white: "#FFFFED";
    readonly orange: "#FF8200";
};
export type DamlogColor = typeof DamlogColor[keyof typeof DamlogColor];
export type DamlogDarkMode = 'light' | 'dark';
export interface DamlogFormattingDirective {
    color?: DamlogColor;
    backgroundColor?: DamlogColor;
    weight?: 'normal' | 'bold';
    modifier?: 'normal' | 'italic';
}
export interface DamlogItemFormatting {
    message?: DamlogFormattingDirective;
    timestamp?: DamlogFormattingDirective;
    level?: DamlogFormattingDirective;
    name?: DamlogFormattingDirective;
}
export interface DamlogFormatting {
    default?: DamlogItemFormatting;
    TRACE?: DamlogItemFormatting;
    DEBUG?: DamlogItemFormatting;
    INFO?: DamlogItemFormatting;
    WARN?: DamlogItemFormatting;
    ERROR?: DamlogItemFormatting;
    FATAL?: DamlogItemFormatting;
}
export declare enum DamlogLevel {
    TRACE = 10,
    DEBUG = 20,
    INFO = 30,
    WARN = 40,
    ERROR = 50,
    FATAL = 60,
    OFF = 100
}
export interface DamlogRecord {
    name: string;
    childName: string;
    level: DamlogLevel;
    time: Date;
    msg: string | object;
}
export type DamlogStreamType = 'console' | 'remote' | 'file' | 'buffer';
export interface DamlogStream {
    write(record: object): void;
}
export interface DamlogConsoleStreamSettings {
    darkMode: DamlogDarkMode;
}
export interface DamlogRemoteStreamSettings {
    loggerUrl: string;
    filePath?: string;
}
export interface DamlogFileStreamSettings {
    filePath: string;
}
export interface DamlogBufferStreamSettings {
    entryLimit: number;
}
export interface DamlogLevelSettings<T> {
    default?: T;
    TRACE?: T;
    DEBUG?: T;
    INFO?: T;
    WARN?: T;
    ERROR?: T;
    FATAL?: T;
}
export interface DamlogSettings {
    console?: {
        level: DamlogLevel;
        settings?: DamlogLevelSettings<DamlogConsoleStreamSettings>;
    };
    remote?: {
        level: DamlogLevel;
        settings?: DamlogLevelSettings<DamlogRemoteStreamSettings>;
    };
    file?: {
        level: DamlogLevel;
        settings?: DamlogLevelSettings<DamlogFileStreamSettings>;
    };
    buffer?: {
        level: DamlogLevel;
        settings?: DamlogLevelSettings<DamlogBufferStreamSettings>;
    };
}
export interface DamlogNamedSettings {
    [key: string]: DamlogSettings;
}
export interface DamlogLogger {
    get name(): string;
    get level(): DamlogLevel;
    get buffer(): string[];
    reset(): void;
    trace(msg: string | object, data?: any): void;
    debug(msg: string | object, data?: any): void;
    warn(msg: string | object, data?: any): void;
    info(msg: string | object, data?: any): void;
    error(msg: string | object, data?: any): void;
    fatal(msg: string | object, data?: any): void;
    alert(msg: string | object): void;
}
//# sourceMappingURL=Damlog.d.ts.map