import { DamlogNamedSettings } from './Damlog';
export interface ResourceConfig {
    extConfigUrl?: string;
    gatherTemplatesUrl?: string;
    parserUrl?: string;
    authUrl?: string;
    authDomain?: string;
    authPoolID?: string;
    authPoolClientID?: string;
}
export interface INamedLogger {
    system: string;
    subsystem: string;
}
type OptionsFlags<Type> = {
    [Property in keyof Type]: boolean;
};
type ParserPayloadFlags = {
    reduce?: () => void;
    encode?: () => void;
    compress?: () => void;
};
export type ParserPayloadOptions = OptionsFlags<ParserPayloadFlags>;
export interface ExtConfig {
    logging?: {
        settings?: DamlogNamedSettings;
        loggers?: INamedLogger[];
    };
    settings?: {
        parserPayloadOptions?: ParserPayloadOptions;
    };
    resources?: ResourceConfig;
    devServerResources?: ResourceConfig;
}
export {};
//# sourceMappingURL=Config.d.ts.map