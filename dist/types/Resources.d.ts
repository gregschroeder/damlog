import { EnvStage } from './EnvStage';
export interface IResources {
    envStage: EnvStage;
}
export interface IFieldTransform {
    pattern: RegExp | string;
    replacement: string | null;
}
export type ProfileType = 'userProfile' | 'companyProfile';
//# sourceMappingURL=Resources.d.ts.map