export interface IProfileEmailAddress {
    address: string;
    type: 'work' | 'home' | 'other';
}
export interface IProfilePhoneNumber {
    number: string;
    type: 'work' | 'home' | 'other';
}
export interface IProfileWebSite {
    url: string;
    type: 'work' | 'home' | 'other';
}
export interface IProfilePosition {
    title?: string;
    company?: string;
    startMonth?: number;
    startYear?: number;
    endMonth?: number;
    endYear?: number;
}
export interface IProfileEducation {
    school?: string;
    degree?: string;
    startYear?: number;
    endYear?: number;
}
export interface IProfileSkill {
    skill?: string;
    endorsementCount?: number;
}
export interface IProfile {
    name?: string;
    profileUrl?: string;
    profilePictureUrl?: string;
    emailAddresses?: IProfileEmailAddress[];
    phoneNumbers?: IProfilePhoneNumber[];
    webSites?: IProfileWebSite[];
    region?: string;
    summary?: string;
    biography?: string;
    positions?: IProfilePosition[];
    education?: IProfileEducation[];
    skills?: IProfileSkill[];
}
//# sourceMappingURL=Profile.d.ts.map