// noinspection JSUnusedGlobalSymbols
export default {
  setupFiles: [
    '<rootDir>/src/__tests__/helpers/jest.setup.ts'
  ],
  automock: false,
  resetMocks: false,
  transform: {
    '^.+\\.ts$': [
      'ts-jest',
      {
        tsconfig: {
          sourceMap: true,
          inlineSourceMap: true
        }
      },
    ],
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: [
    '**/*.test.ts',
  ],
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts',
    '!**/node_modules/**',
    '!**/coverage/**',
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/',
  ],
  coverageReporters: [
    'json',
    'lcov',
    'text',
    'html'
  ],
  coverageThreshold: {
    'global': {
      'branches': 80,
      'functions': 100,
      'lines': 100,
      'statements': 100
    }
  },
};
