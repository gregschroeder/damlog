# Log Common

Common utilities and types for logging

## Logging

Damlog is logging system supporting the following concepts:
* Named (subsystem, component) logs
* Per log-event logging levels attached to the message
* Per logger logging levels that filter messages that are considered noisier than this channel will currently emit
* Output streams
  * Console (both terminal/NodeJS and browser/devtools)
  * Remote (logRelay to a server or other HTTP recipient)
  * File (typically used by the server when relaying)
  * Future: ELK Beats, etc
* Hierarchy of settings
  * Defaults
    * global defaults
    * per subsystem (name)
    * per stream type
  * Overrides
    * global overrides
    * per subsystem
    * per stream type

### Execution Spaces
In the browser, extensions have several such spaces - the background worker,
the content script, the popup, the options, etc. Each such space is a separate
sandbox from the other, and so, what appears to be a single application is
not and cannot share state with memory variables. Background workers can be 
swept out of service and lose state. This means that there needs to be application
level awareness of this, and the use of techniques like saving to local
storage and broadcasting updates to globals (like a configuration setting). 
This impacts the design of systems like logging for which there should be
centralized managed settings (like, which logging is enabled at what level).

### Streams
Streams are centrally managed per execution space. Each execution space
has one global handler with one of each stream type instantiated.  So, for
example, all named loggers within that space that include a console stream 
in fact reference the _same_ console stream.

All loggers are associated with all streams, but settings dictate which ones
are used at emit time.

### Logger Proxies and Centralized Settings
Settings are kept in memory (as well as local storage in the browser), and
this includes logging settings. This hierarchy of settings (base per stream and overrides) 
dictates which named loggers are active at which logging level.

The logger level is not set at object instantiation time. Rather, each logger
is wrapped in a proxy, as part of the stream dispatching management, directly 
queries the global settings. Therefore, whenever those settings are updated, they are
immediately visible and put to use by all consuming loggers.  When there are separate
execution spaces, this is still true, although there is an additional need to
broadcast across the spaces to keep all of those config instances in sync.


[![JetBrains](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg)](https://www.jetbrains.com/)


