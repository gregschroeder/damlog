import {
  DamlogColor,
  DamlogDarkMode,
  DamlogFormatting,
  DamlogFormattingDirective,
  DamlogItemFormatting,
  DamlogLevel,
  DamlogRecord,
  DamlogSettings,
  DamlogStream
} from '../types/Damlog';
import {Damlog} from './Damlog';
import chalk from 'chalk';

// only consoles will support formatting
// logs are generally JSON and should not have control characters

export const DAMLOG_DEFAULT_FORMATTING: { [key in DamlogDarkMode]: DamlogFormatting } = {
  light: {
    default: {
      message: {color: DamlogColor.black},
      timestamp: {color: DamlogColor.gray, modifier: 'italic'},
      level: {color: DamlogColor.cyan},
      name: {color: DamlogColor.blue},
    },
    TRACE: {
      message: {color: DamlogColor.magenta},
      level: {color: DamlogColor.cyan, weight: 'bold'},
    },
    DEBUG: {
      message: {color: DamlogColor.green},
      level: {color: DamlogColor.green, weight: 'bold'},
    },
    INFO: {
      message: {color: DamlogColor.black},
      level: {color: DamlogColor.black},
      name: {color: DamlogColor.black},
    },
    WARN: {
      message: {color: DamlogColor.orange},
      level: {color: DamlogColor.orange, weight: 'bold'},
    },
    ERROR: {
      message: {color: DamlogColor.red},
      level: {color: DamlogColor.red, weight: 'bold'},
    },
    FATAL: {
      message: {color: DamlogColor.red},
      level: {color: DamlogColor.yellow, backgroundColor: DamlogColor.red, weight: 'bold'},
    },
  },
  dark: {
    default: {
      message: {color: DamlogColor.white},
      timestamp: {color: DamlogColor.gray, modifier: 'italic'},
      level: {color: DamlogColor.cyan},
      name: {color: DamlogColor.blue},
    },
    TRACE: {
      message: {color: DamlogColor.magenta},
      level: {color: DamlogColor.cyan, weight: 'bold'},
    },
    DEBUG: {
      message: {color: DamlogColor.green},
      level: {color: DamlogColor.green, weight: 'bold'},
    },
    INFO: {
      message: {color: DamlogColor.white},
      level: {color: DamlogColor.white},
      name: {color: DamlogColor.white},
    },
    WARN: {
      message: {color: DamlogColor.orange},
      level: {color: DamlogColor.orange, weight: 'bold'},
    },
    ERROR: {
      message: {color: DamlogColor.red},
      level: {color: DamlogColor.red, weight: 'bold'},
    },
    FATAL: {
      message: {color: DamlogColor.yellow},
      level: {color: DamlogColor.yellow, backgroundColor: DamlogColor.red, weight: 'bold'},
    },
  },
};

export class DamlogConsoleStream implements DamlogStream {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  private _inBrowser: boolean = typeof process === 'undefined';

  private static padZeroes(number: number, len: number): string {
    return Array((len + 1) - (number + '').length).join('0') + number;
  }

  private formatItemChalk(item: string, format: DamlogFormattingDirective, logDirective: string[]): void {
    let result = item;

    // apply color
    if (format.color) {
      const color = `${format.color}`;
      if (color.startsWith('#')) {
        result = chalk.hex(color)(result);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        result = (<any>chalk)[color](result);
      }
    }

    // apply background color
    if (format.backgroundColor) {
      const backgroundColor = `${format.backgroundColor}`;
      if (backgroundColor.startsWith('#')) {
        result = chalk.bgHex(backgroundColor)(result);
      } else {
        const bgColor = `bg${backgroundColor.charAt(0).toUpperCase()}${backgroundColor.substring(1).toLowerCase()}`;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        result = (<any>chalk)[bgColor](result);
      }
    }

    // apply weight
    if (format.weight === 'bold') {
      result = chalk.bold(result);
    }

    // apply modifier
    if (format.modifier === 'italic') {
      result = chalk.italic(result);
    }

    logDirective.push(result);
  }

  private formatItemCSS(item: string, format: DamlogFormattingDirective, logDirective: string[], logArgs: string[]): void {
    const cssArgs = [];

    // apply color
    if (format.color) {
      cssArgs.push(`color: ${format.color}`);
    }

    // apply background color
    if (format.backgroundColor) {
      cssArgs.push(`background-color: ${format.backgroundColor}`);
    }

    // apply weight
    if (format.weight === 'bold') {
      cssArgs.push('font-weight: bold');
    }

    // apply modifier
    if (format.modifier === 'italic') {
      cssArgs.push('font-style: italic');
    }

    if (cssArgs.length > 0) {
      // add %c to directive and args to arg list
      logDirective.push('%c');
      logArgs.push(cssArgs.join('; '));

      // add %s to directive and item string to arg list
      logDirective.push('%s');
      logArgs.push(item);
    } else {
      // no css, just push item inline in directive
      logDirective.push(item);
    }
  }

  private formatItem(item: string, format: DamlogFormattingDirective, logDirective: string[], logArgs: string[]): void {
    if (this._inBrowser) {
      return this.formatItemCSS(item, format, logDirective, logArgs);
    }
    return this.formatItemChalk(item, format, logDirective); // args not needed; all strings inline in directive
  }

  // format: [time] level: loggerName: msg
  write(rec: DamlogRecord): void {
    // timestamp
    const hours = DamlogConsoleStream.padZeroes(rec.time.getHours(), 2);
    const min = DamlogConsoleStream.padZeroes(rec.time.getMinutes(), 2);
    const sec = DamlogConsoleStream.padZeroes(rec.time.getSeconds(), 2);
    const ms = DamlogConsoleStream.padZeroes(rec.time.getMilliseconds(), 4);
    const time = `[${hours}:${min}:${sec}.${ms}]`;

    // labels
    const levelName = Damlog.stringFromLevel(rec.level);
    const loggerName = rec.childName ? rec.name + '/' + rec.childName : rec.name;

    // output function to use
    let consoleMethodLevel = levelName;
    if (rec.level === DamlogLevel.DEBUG || rec.level === DamlogLevel.TRACE) {
      // for tracing/debugging, in dev, use console.info() in browser, special code later to get 'details' with stacktrace
      // NB: process.env.DAMLOG_ENV substituted by webpack at build time
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      consoleMethodLevel = 'info';
    } else if (rec.level === DamlogLevel.ERROR || rec.level === DamlogLevel.FATAL) {
      // for errors, use console.error() in browser, but not in terminal
      consoleMethodLevel = this._inBrowser ? 'error' : 'log';
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const consoleMethod = typeof (<any>console)[consoleMethodLevel] === 'function' ? (<any>console)[consoleMethodLevel] : console.log;

    // apply level specific formatting
    const settings: DamlogSettings = Damlog.configSettings(rec.name);
    const darkMode = settings?.console?.settings?.default?.darkMode || 'dark';
    const baseFormatting = DAMLOG_DEFAULT_FORMATTING[darkMode];

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const levelFormatting = (<any>baseFormatting)[levelName];

    const formatting: DamlogItemFormatting = Object.assign({}, baseFormatting.default, levelFormatting);

    // formulate directive string
    // [time] level: loggerName: msg obj
    const logDirective: string[] = [];
    const logArgs: any[] = []; // eslint-disable-line @typescript-eslint/no-explicit-any
    this.formatItem(time, <DamlogFormattingDirective>formatting.timestamp, logDirective, logArgs);
    logDirective.push(' ');
    this.formatItem(levelName, <DamlogFormattingDirective>formatting.level, logDirective, logArgs);
    logDirective.push(' ');
    this.formatItem(loggerName, <DamlogFormattingDirective>formatting.name, logDirective, logArgs);
    logDirective.push(' ');
    this.formatItem('%s', <DamlogFormattingDirective>formatting.message, logDirective, logArgs);

    // put logging command first, then any formatting args, then message and object arguments
    logArgs.unshift(logDirective.join(''));
    logArgs.push(rec.msg);

    // add any remaining unknown arguments as objects to log
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    for (const [key, value] of Object.entries(rec)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (!['name', 'level', 'levelName', 'msg', 'time', 'v'].includes(key)) {
        const obj = {};
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (<any>obj)[key] = value;
        logArgs.push(obj);
      }
    }

    consoleMethod.apply(console, logArgs);

    // in browser, follow with collapsed group with trace() call to get sourcemapped stack track
    // will include logging subsystem but oh well...
    if (this._inBrowser) {
      console.groupCollapsed('');
      console.trace('stack');
      console.groupEnd();
    }
  }
}
