import {DamlogBufferStreamSettings, DamlogRecord, DamlogStream} from '../types/Damlog';
import {Damlog} from './Damlog';

const ENTRY_LIMIT = 500;

export class DamlogBufferStream implements DamlogStream {
  protected _buffer: string[];

  constructor(buffer: string[]) {
    this._buffer = buffer;
  }

  write(rec: DamlogRecord): void {
    const logError = (err: unknown) => {
      /* istanbul ignore next */
      console.error('error attempting to log buffer stream', err);
    };

    // aim to never throw an error from a logging system, so put whole thing in a try/catch
    try {
      // get file path from environment for this name
      let entryLimit: number = ENTRY_LIMIT;

      // look in a name specific see if we find a level-specific setting or default,
      const settings = Damlog.configSettings(rec.name)?.buffer?.settings;
      if (settings) {
        const levelName = Damlog.stringFromLevel(rec.level);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const levelSettings: DamlogBufferStreamSettings = settings[levelName];
        entryLimit = levelSettings?.entryLimit;
      }

      // failing that, look for the default
      if (!entryLimit) {
        if (settings) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const levelSettings: DamlogBufferStreamSettings = settings['default'];
          entryLimit = levelSettings?.entryLimit;
        }
      }

      // add entry
      this.writeBuffer(rec);

      // prune buffer
      this.limitBuffer(entryLimit);
    } catch (err) {
      /* istanbul ignore next */
      logError(err);
      /* istanbul ignore next */
      return;
    }
  }

  protected writeBuffer(rec: DamlogRecord) {
    // add entry
    this._buffer.push(JSON.stringify(rec));
  }

  protected limitBuffer(entryLimit: number) {
    while(this._buffer.length > (entryLimit || ENTRY_LIMIT)) {
      this._buffer.shift();
    }
  }
}
