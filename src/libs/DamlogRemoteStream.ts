import {DamlogRecord, DamlogStream} from '../types/Damlog';
import {Damlog} from './Damlog';
import {DamHttpClient} from './DamHttpClient';

interface LogRelayResponseMessage {
  message: string,
}

export class DamlogRemoteStream implements DamlogStream {
  write(rec: DamlogRecord): void {
    const logError = (err: unknown) => {
      console.error('error attempting to log remote stream', err);
    };

    // aim to never throw an error from a logging system, so put whole thing in a try/catch
    try {
      // get loggerUrl and optional filePath from environment for this name
      let loggerUrl: string | null = null;
      let filePath: string | null = null;

      // look in a name specific see if we find a level-specific setting or default,
      const settings = Damlog.configSettings(rec.name)?.remote?.settings;
      if (settings) {
        const levelName = Damlog.stringFromLevel(rec.level);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const levelSettings: DamlogRemoteStreamSettings = settings[levelName];
        loggerUrl = levelSettings?.loggerUrl;
        filePath = levelSettings?.filePath;
      }

      // failing that, look for the default
      if (!loggerUrl) {
        filePath = null;
        if (settings) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const levelSettings: DamlogRemoteStreamSettings = settings['default'];
          loggerUrl = levelSettings?.loggerUrl;
          filePath = levelSettings?.filePath;
        }
      }

      if (!loggerUrl) {
        logError(new Error('unable to find loggerUrl in settings'));
        return;
      }

      // get any unknown args, relay separately in dataStr
      const logArgs: unknown[] = [];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      for (const [key, value] of Object.entries(rec)) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (!['name', 'level', 'levelName', 'msg', 'time', 'v'].includes(key)) {
          const obj = {};
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (<any>obj)[key] = value;
          logArgs.push(obj);
        }
      }

      const body = {
        name: rec.name,
        level: Damlog.stringFromLevel(rec.level),
        msg: rec.msg,
      };

      if (logArgs.length) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        body.dataStr = logArgs.length === 1 ? JSON.stringify(logArgs[0]) : JSON.stringify(logArgs);
      }

      if (filePath) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        body.filePath = filePath;
      }

      const httpClient = new DamHttpClient();

      // fetch(loggerUrl, {
      //   method: 'POST',
      //   headers: {
      //     'Accept': 'application/json',
      //     'Content-Type': 'application/json',
      //     'Pragma': 'no-cache',
      //     'Cache-Control': 'no-cache',
      //     // TODO: where to get auth headers?
      //   },
      //   body: JSON.stringify(body),
      // }).then(response => response.json())
      httpClient.post<string, LogRelayResponseMessage>({url: loggerUrl, noCache: true}, JSON.stringify(body))
        .then((response: LogRelayResponseMessage | string) => {
          const body = <LogRelayResponseMessage>response;
          /* istanbul ignore next */
          if (!body?.message?.includes(`logged ${rec.name}`)) {
            if (body?.message) {
              throw new Error(body.message);
            } else {
              throw new Error('missing response message');
            }
          }
        })
        .catch((err) => {
          logError(err);
        });
    } catch (err) {
      /* istanbul ignore next */
      logError(err);
      /* istanbul ignore next */
      return;
    }
  }
}
