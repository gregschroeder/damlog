import {createLogger, Logger, TRACE} from 'browser-bunyan';
import {
  DamlogLevel,
  DamlogLogger,
  DamlogNamedSettings,
  DamlogRecord,
  DamlogSettings,
  DamlogStream,
  DamlogStreamType
} from '../types/Damlog';
import {DamlogConsoleStream} from './DamlogConsoleStream';
import Config from './Config';
import {DamlogFileStream} from './DamlogFileStream';
import {DamlogRemoteStream} from './DamlogRemoteStream';
import {DamlogBufferStream} from './DamlogBufferStream';

// fallback settings if none are found in Config
// also serves as a somewhat easier documentation example than TypeScript type navigation
const DEFAULT_DAMLOG_SETTINGS: DamlogNamedSettings = {
  default: {
    console: {
      level: DamlogLevel.OFF,
      settings: {
        default: {
          darkMode: 'dark',
        },
      },
    },
    remote: {
      level: DamlogLevel.OFF,
      settings: {
        default: {
          loggerUrl: 'http://localhost:3000/dev/logRelay',
          filePath: '/tmp/damli-remote.log',
        },
        // other levels would go here, to say, put tracing in a different place
        // TRACE: { ... }
      }
    },
    file: {
      level: DamlogLevel.OFF,
      settings: {
        default: {
          filePath: '/tmp/damli.log',
        },
        // other levels would go here, to say, put tracing in a different place
        // TRACE: { ... }
      }
    },
    buffer: {
      level: DamlogLevel.OFF,
      settings: {
        default: {
          entryLimit: 500,
        },
      }
    },
  }
};

// global in-memory log buffer
const logBuffer: string[] = [];

type OutputStream = { [key in DamlogStreamType]?: DamlogStream };
const defaultDamlogOutputStreams: OutputStream = {
  console: new DamlogConsoleStream(),
  remote: new DamlogRemoteStream(),
  file: new DamlogFileStream(),
  buffer: new DamlogBufferStream(logBuffer),
};

export class Damlog implements DamlogLogger {
  private readonly _name: string;
  private readonly _logger: Logger;

  static stringFromLevel(level: DamlogLevel): string {
    return DamlogLevel[level];
  }

  static levelFromString(val: string): DamlogLevel {
    if (val === null) {
      throw new Error('Argument must be set');
    }

    switch (val.toLowerCase()) {
      case 'trace':
        return DamlogLevel.TRACE;
      case 'debug':
        return DamlogLevel.DEBUG;
      case 'info':
        return DamlogLevel.INFO;
      case 'warn':
        return DamlogLevel.WARN;
      case 'error':
        return DamlogLevel.ERROR;
      case 'fatal':
        return DamlogLevel.FATAL;
      case 'off':
        return DamlogLevel.OFF;
      default:
        throw new Error('Unsupported value for conversion: ' + val);
    }
  };

  static createLogger(name: string, overrideOutputStreams?: OutputStream): DamlogLogger {
    // determine output streams
    const outputStreams: OutputStream = Object.assign({}, defaultDamlogOutputStreams, overrideOutputStreams || {});

    // create stream dispatcher for this name
    const streamDispatcher = Damlog.streamDispatcher(name, outputStreams);

    // create the underlying Bunyan logger.
    // set to trace so at its level it logs everything,
    // and the actual level check happening in the dispatcher
    const bunyanLogger: Logger = createLogger({name, stream: streamDispatcher, level: TRACE, src: false});

    // instantiate and return our wrapper
    return new Damlog(name, bunyanLogger);
  }

  // create a stream dispatcher that has all available output streams,
  // and looks up the config to determine which one(s) to delegate the writes to
  static streamDispatcher(name: string, outputStreams: OutputStream) {
    const dispatcher: DamlogStream = {
      write(record: DamlogRecord): void {
        const settings = Damlog.configSettings(name);
        if (settings) {
          for (const streamType of ['console', 'remote', 'file', 'buffer']) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            if (settings[streamType] && record.level >= settings[streamType].level && outputStreams[streamType]) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              outputStreams[streamType].write(record);
            }
          }
        }
      }
    };

    return dispatcher;
  }

  // return the settings for the named logger, applying the inheritance hierarchy
  // - named values < 'default' values
  // - config env < default builtin
  static configSettings(name: string): DamlogSettings {
    const defaultSettings: DamlogSettings = Config.env?.logging?.settings?.default || {};
    const namedSettings: DamlogSettings = Config.env?.logging?.settings?.[name] || {};

    // merge results in precedence order
    const result = <DamlogSettings>Object.assign(DEFAULT_DAMLOG_SETTINGS.default, defaultSettings, namedSettings);
    return result || {};
  }

  private constructor(name: string, logger: Logger) {
    this._name = name;
    this._logger = logger;
  }

  get name(): string {
    return this._name;
  }

  get level(): DamlogLevel {
    return this._logger.level();
  }

  get buffer(): string[] {
    // shared global buffer
    return logBuffer;
  }

  reset(): void {
    logBuffer.length = 0;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _log(method: string, msg: string | object, data?: any): void {
    if (typeof msg === 'string' && typeof data === 'object') {
      // Bunyan has a bizarre rule that has the object first and then the string,
      // in order to let the console do its object display, which is preferable
      // to just appending a stringified value.
      // so we intercept those here and swap them back for better functionality.

      // nest data in object to avoid collisions on the top level of objects,
      // where the log record has common properties like "name"

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (<any>this._logger)[method]({data}, msg);
    } else if (typeof data !== 'undefined') {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (<any>this._logger)[method](msg, {data});
    } else {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (<any>this._logger)[method](msg);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trace(msg: string | object, data?: any): void {
    this._log('trace', msg, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  debug(msg: string | object, data?: any): void {
    this._log('debug', msg, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  warn(msg: string | object, data?: any): void {
    this._log('warn', msg, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  info(msg: string | object, data?: any): void {
    this._log('info', msg, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error(msg: string | object, data?: any): void {
    this._log('error', msg, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fatal(msg: string | object, data?: any): void {
    this._log('fatal', msg, data);
  }

  alert(msg: string): void {
    this._log('error', `ALERT: ${msg}`);
    const theWindow = this._window();
    if (typeof theWindow !== 'undefined' && typeof theWindow.alert === 'function') {
      theWindow.alert(msg);
    }
  }

  // separate so mockable for testing
  _window(): Window | undefined {
    return typeof window !== 'undefined' ? window : undefined;
  }
}
