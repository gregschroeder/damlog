export type DamHttpHeaders = HeadersInit;

export interface DamHttpRequestOptions {
  url: string,
  method?: string,
  headers?: DamHttpHeaders, // headers to send
  noCache?: boolean, // add no-cache headers
  noCors?: boolean, // set no-cors mode
  payloadAsParams?: boolean, // send payload as parameter string
  asText?: boolean, // return result as text string instead of json'd object
}

// our request object is a superset of fetch request, merging in our options
export type DamHttpClientRequest = DamHttpRequestOptions & RequestInit;
export type DamHttpClientResponse = Response;

// as much context as we can provide on error, similar to Axios pattern of:
// - response : if present, has error response (4xx, 5xx)
// - request : didn't get a response, but this the request that failed
// - error : some other error
export interface DamHttpClientErrorResponse {
  status?: number,
  response?: DamHttpClientResponse,
  request?: DamHttpClientRequest,
  error?: unknown,
}

type DamHookType = 'request' | 'response';
export type DamHttpClientRequestHookHandler = (request: DamHttpClientRequest) => Promise<DamHttpClientRequest>;
export type DamHttpClientResponseHookHandler = (request: DamHttpClientRequest, response: DamHttpClientResponse) => Promise<DamHttpClientResponse>;

export interface DamHttpClientHook<T> {
  id: number,
  handler: T,
}

const requestHooks: DamHttpClientHook<DamHttpClientRequestHookHandler>[] = [];
const responseHooks: DamHttpClientHook<DamHttpClientResponseHookHandler>[] = [];

export class DamHttpClient {
  private static _addHook<T>(
    type: DamHookType,
    handler: T,
  ): number {
    const hooks = type === 'request' ? requestHooks : responseHooks;
    const id = hooks.length ? hooks.sort((a, b) => a.id - b.id)[0].id + 1 : 1;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    hooks.push({id, handler: <any>handler});
    return id;
  }

  private static _removeHook(type: DamHookType, handlerId: number): void {
    const hooks = type === 'request' ? requestHooks : responseHooks;
    const index = hooks.findIndex((hook) => hook.id === handlerId);
    if (index >= 0) {
      hooks.splice(index, 1);
    }
  }

  static addRequestHook(handler: DamHttpClientRequestHookHandler): number {
    return DamHttpClient._addHook('request', handler);
  }

  static removeRequestHook(handlerId: number): void {
    DamHttpClient._removeHook('request', handlerId);
  }

  static addResponseHook(handler: DamHttpClientResponseHookHandler): number {
    return DamHttpClient._addHook('response', handler);
  }

  static removeResponseHook(handlerId: number): void {
    DamHttpClient._removeHook('response', handlerId);
  }

  static addAuthRefreshTokenHook(
    refreshTokenFunc: (request: DamHttpClientRequest, response: DamHttpClientResponse) => Promise<string>,
  ): number {
    const authRefreshTokenHandler: DamHttpClientResponseHookHandler = async (request: DamHttpClientRequest, response: DamHttpClientResponse): Promise<DamHttpClientResponse> => {
      if (response?.status === 401) {
        const token: string = await refreshTokenFunc(request, response);
        request.headers = {...request.headers, ...{'Authorization': `Bearer ${token}`}};

        // call fetch again, directly, and not via our wrapper,
        // so no repeat of hooks and no recursion
        return fetch(request.url, request);
      }

      // just pass through in other cases
      return Promise.resolve(response);
    };

    return DamHttpClient.addResponseHook(authRefreshTokenHandler);
  }

  protected async _action<ReqType, RespType>(opts: DamHttpRequestOptions, payload?: ReqType): Promise<RespType | string> {
    const noCacheHeaders: DamHttpHeaders = opts.noCache ? {
      'Pragma': 'no-cache',
      'Cache-Control': 'no-cache',
    } : {};

    const actionOpts: DamHttpRequestOptions = {...opts};
    actionOpts.headers = {
      ...{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      ...noCacheHeaders,
      ...opts.headers || {}
    };

    // make pseudo request object
    let request: DamHttpClientRequest = actionOpts;

    if (opts.noCors) {
      request.mode = 'no-cors';
    }

    if (opts.method === 'POST' && payload) {
      if (opts.payloadAsParams) {
        const params = new URLSearchParams(<string[][]>payload);
        request.body = params.toString();
      } else {
        request.body = JSON.stringify(payload);
      }
    }

    // process request hooks, assume the most recent addition is the highest priority
    for (const hook of requestHooks.reverse()) {
      try {
        request = await hook.handler(request);
      } catch (error) {
        const errorResponse: DamHttpClientErrorResponse = {
          request,
          error,
        };
        return Promise.reject(errorResponse);
      }
    }

    // perform fetch action
    let response: Response = await fetch(request.url, request)
      .catch(error => {
        const errorResponse: DamHttpClientErrorResponse = {
          request,
          error,
        };
        return Promise.reject(errorResponse);
      });

    // process response hooks, assume the most recent addition is the highest priority
    for (const hook of responseHooks.reverse()) {
      try {
        response = await hook.handler(request, response);
      } catch (error) {
        const errorResponse: DamHttpClientErrorResponse = {
          status: response.status,
          request,
          response,
          error,
        };
        return Promise.reject(errorResponse);
      }
    }

    // treat http error status codes as client errors
    if (response.status >= 400) {
      const errorResponse: DamHttpClientErrorResponse = {
        status: response.status,
        request,
        response,
      };
      return Promise.reject(errorResponse);
    }

    // return body as text, if requested
    if (opts.asText) {
      const body = await response.text()
        .catch(error => {
          const errorResponse: DamHttpClientErrorResponse = {
            status: response.status,
            request,
            response,
            error,
          };
          return Promise.reject(errorResponse);
        });

      return Promise.resolve(body);
    }

    // default is JSON, parsed into object
    const data = response.json()
      .catch(error => {
        const errorResponse: DamHttpClientErrorResponse = {
          status: response.status,
          request,
          response,
          error,
        };
        return Promise.reject(errorResponse);
      });
    return Promise.resolve(data);
  }

  get<RespType>(opts: DamHttpRequestOptions | string): Promise<RespType | string> {
    let options: DamHttpRequestOptions;
    if (typeof opts === 'string') {
      options = {url: opts, method: 'GET'};
    } else {
      options = opts;
    }
    options.method = 'GET';
    return this._action(options);
  }

  post<ReqType, RespType>(opts: DamHttpRequestOptions | string, payload: ReqType): Promise<RespType | string> {
    let options: DamHttpRequestOptions;
    if (typeof opts === 'string') {
      options = {url: opts, method: 'POST'};
    } else {
      options = opts;
    }
    options.method = 'POST';
    return this._action(options, payload);
  }
}
