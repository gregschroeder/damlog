import {DamlogFileStreamSettings, DamlogRecord, DamlogStream} from '../types/Damlog';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import fs from 'fs';
import {Damlog} from './Damlog';

// obviously not supported in browsers
// webpack will thunk out fs

export class DamlogFileStream implements DamlogStream {
  write(rec: DamlogRecord): void {
    const logError = (err: unknown) => {
      console.error('error attempting to log filestream', err);
    };

    // aim to never throw an error from a logging system, so put whole thing in a try/catch
    try {
      // get file path from environment for this name
      let filePath: string | null = null;

      // look in a name specific see if we find a level-specific setting or default,
      const settings = Damlog.configSettings(rec.name)?.file?.settings;
      if (settings) {
        const levelName = Damlog.stringFromLevel(rec.level);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const levelSettings: DamlogFileStreamSettings = settings[levelName];
        filePath = levelSettings?.filePath;
      }

      // failing that, look for the default
      if (!filePath) {
        if (settings) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const levelSettings: DamlogFileStreamSettings = settings['default'];
          filePath = levelSettings?.filePath;
        }
      }

      if (!filePath) {
        logError(new Error('unable to find filePath in settings'));
        return;
      }

      // convert output to string
      const output = JSON.stringify(rec);
      if (output) {
        fs.appendFileSync(filePath, output);
      }
    } catch (err) {
      logError(err);
      return;
    }
  }
}
