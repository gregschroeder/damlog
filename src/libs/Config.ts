import {EnvStage} from '../types/EnvStage';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import fs from 'fs';
import {SimpleCrypto} from 'simple-crypto-js';
import {ParserPayloadOptions} from '../types/Config';
import {DamHttpClient} from './DamHttpClient';

declare let process: {
  env: {
    [key: string]: string,
  }
};

interface EnvType {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any,
}

export default class Config {
  private static _instance: Config;
  private _env: EnvType = {};

  constructor(noInit = false) {
    if (Config._instance) {
      return Config._instance;
    }

    Config._instance = this;
    if (!noInit) {
      Config.initConfig();
    }
  }

  static async initConfig(overrides = {}, overwrite = true): Promise<Config> {
    const me = Config._instance || new Config(true);

    // copy any process.env values
    if (typeof process !== 'undefined' && process.env) {
      Object.assign(me._env, process.env);
    }

    // add overrides, if any
    Object.assign(me._env, overrides);

    // if DAMLOG_ENV_URI is set, load JSON config from there
    const uri = me._env.DAMLOG_ENV_URI;
    if (uri) {
      try {
        await me.loadURIEnvConfig(uri, overwrite);
      } catch (err) {
        console.log(`error loading config from ${uri}`, err);
        throw err;
      }
    }

    // fallback to NODE_ENV if DAMLOG_ENV not set
    const node_env = me._env.NODE_ENV;
    if (!me._env.DAMLOG_ENV) {
      if (node_env) {
        let DAMLOG_ENV: EnvStage = 'dev';

        if (node_env.startsWith('prod')) {
          DAMLOG_ENV = 'prod';
        } else if (node_env === 'test') {
          DAMLOG_ENV = 'test';
        }

        me._env.DAMLOG_ENV = DAMLOG_ENV;
      } else {
        me._env.DAMLOG_ENV = 'dev';
      }
    }

    Config._instance = me;
    return me;
  }

  public get env(): EnvType {
    return this._env;
  }

  public static get instance(): Config {
    return new Config();
  }

  public static _resetInstance(): void {
    // for unit testing purposes, reset the singleton
    if (Config._instance) {
      Object.assign(Config._instance._env, {});
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      Config._instance = undefined;
    }
  }

  public static get env(): EnvType {
    return Config.instance._env;
  }

  static envStage(): EnvStage {
    // return configured stage, defaults to dev
    const DAMLOG_ENV = Config.env.DAMLOG_ENV;
    return <EnvStage>DAMLOG_ENV;
  }

  // noinspection JSUnusedGlobalSymbols
  static isProduction(): boolean {
    return Config.envStage() === 'prod';
  }

  // noinspection JSUnusedGlobalSymbols
  static isDev(): boolean {
    return Config.envStage() === 'dev';
  }

  // noinspection JSUnusedGlobalSymbols
  static messages(): {[key: string]: string} {
    return {
      extConfig: 'Copyright - All Rights Reserved',
      auth: 'Authentication: Basic',
      gatherTemplates: 'IGatherTemplates for parsing',
      grabber: 'Grabber IGrabberTemplate IMetaData',
      parser: 'Profile and other data parser',
    };
  }

  static parserDataOpts(flags: string): ParserPayloadOptions {
    return {
      reduce: flags?.indexOf('r') >= 0, // not relevant in transmission but..
      encode: flags?.indexOf('e') >= 0,
      compress: flags?.indexOf('c') >= 0,
    };
  }

  static parserDataOptsStr(opts: ParserPayloadOptions): string {
    return `${opts.reduce ? 'r' : ''}${opts.encode ? 'e' : ''}${opts.compress ? 'c' : ''}`;
  }

  // noinspection JSUnusedGlobalSymbols
  static forSend(message: string, payload: any): string { // eslint-disable-line @typescript-eslint/no-explicit-any
    const encr = new SimpleCrypto(message);
    return encr.encrypt(payload);
  }

  // noinspection JSUnusedGlobalSymbols
  static forReceipt(message: string, payload: string): any { // eslint-disable-line @typescript-eslint/no-explicit-any
    const encr = new SimpleCrypto(message);
    return encr.decrypt(payload);
  }

  // load config from URI
  async loadURIEnvConfig(uri: string, overwrite = true): Promise<void> {
    if (uri.startsWith('http')) {
      const httpClient = new DamHttpClient();
      const body = await httpClient.get<string>({url: uri, noCache: true, asText: true});
      if (body) {
        const bodyObj = Config.forReceipt(Config.messages().extConfig, body);
        if (bodyObj) {
          this.loadEnvConfig(bodyObj, overwrite);
        }
      }
    } else if (uri.startsWith('file://') || uri.startsWith('/') || uri.startsWith('./') || uri.startsWith('../')) {
      const path = uri.replace(/^file:\/+/, '').replace(/\?.*/, '');
      if (fs.existsSync(path)) {
        const data: string = fs.readFileSync(path, {encoding: 'utf-8'});
        this.loadJSONEnvConfig(data, overwrite);
      }
    }
  }

  // load JSON config into config env
  loadJSONEnvConfig(jsonString: string, overwrite = true): void {
    this.loadEnvConfig(JSON.parse(jsonString), overwrite);
  }

  // load env object config into config env
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  loadEnvConfig(envConfig: any, overwrite = true): void {
    for (const key in envConfig) {
      // if value exists, merge in the new values, otherwise add it
      if (this._env[key]) {
        if (overwrite) {
          this._env[key] = Config.mergeDeep(this._env[key], envConfig[key]);
        } else {
          this._env[key] = Config.mergeDeep(envConfig[key], this._env[key]);
        }
      } else {
        this._env[key] = envConfig[key];
      }
    }
  }

  // replace env object config
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setEnvConfig(envConfig: any): void {
    this._env = envConfig;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static isObject(item: any) {
    return (item && typeof item === 'object' && !Array.isArray(item));
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static mergeDeep(target: any, ...sources: any): any {
    if (!sources.length) {
      return target;
    }
    const source = sources.shift();

    if (Config.isObject(target) && this.isObject(source)) {
      for (const key in source) {
        if (this.isObject(source[key])) {
          if (!target[key]) {
            Object.assign(target, { [key]: {} });
          }
          Config.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }

    return Config.mergeDeep(target, ...sources);
  }
}
