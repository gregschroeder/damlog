import {Damlog} from '../../../libs/Damlog';
import {DamlogLevel} from '../../../types/Damlog';


describe('Damlog', () => {
  const name = 'SubSystem';

  beforeEach(() => {
    // Logger._reset();
    //jest.spyOn(console, 'log').mockImplementation(jest.fn());
    jest.spyOn(console, 'debug').mockImplementation(jest.fn());
    jest.spyOn(console, 'error').mockImplementation(jest.fn());
  });

  afterEach(() => {
    jest.restoreAllMocks();
    // Logger._reset();
  });

  describe('Damlog levels', () => {
    it('should have numeric level to and from level string conversion', () => {
      for (const level in DamlogLevel) {
        if (isNaN(Number(level))) {
          // given a string value, get the number
          const num = Damlog.levelFromString(level);

          // verify the conversion matches in the other direction
          expect(Damlog.stringFromLevel(num)).toEqual(level);

          // sample a couple just to make sure things look good and not just to/from junk
          // not doing them all, or it essentially duplicates the code being tested
          if (num === DamlogLevel.DEBUG) {
            expect(level).toEqual('DEBUG');
          }
          if (level === 'ERROR') {
            expect(num).toEqual(DamlogLevel.ERROR);
          }
        }
      }

      // expect error if null input given
      expect(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        Damlog.levelFromString(null);
      }).toThrowError();

      // expect error if bogus input given
      expect(() => {
        Damlog.levelFromString('bogus');
      }).toThrowError();
    });
  });

  describe('emitters', () => {
    it('should call wrapped logger routine', () => {
      const log = Damlog.createLogger(name);
      const logMessage = 'test log proxy call';
      const logData = {data: 1234};

      ['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'alert'].forEach((fn) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const spy = jest.spyOn(Damlog.prototype, fn).mockImplementation(jest.fn());

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        log[fn](logMessage, logData);
        expect(spy).toHaveBeenCalledWith(logMessage, logData);
      });
    });

    it('should call alert', () => {
      const log = Damlog.createLogger(name);
      const msg = 'test log proxy call';

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const logSpy = jest.spyOn(Damlog.prototype, '_log');

      log.alert(msg);
      expect(logSpy).toHaveBeenCalledWith('error', `ALERT: ${msg}`);

      const myAlert = jest.fn();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      jest.spyOn(log, '_window').mockImplementation(() => {
        return {
          alert: myAlert,
        };
      });
      log.alert(msg);
      expect(logSpy).toHaveBeenCalledWith('error', `ALERT: ${msg}`);
      expect(myAlert).toHaveBeenCalledWith(msg);
    });
  });
});
