import Config from '../../../libs/Config';
import {disableFetchMocks, enableFetchMocks} from 'jest-fetch-mock';
import path from 'path';
import fs from 'fs';

describe('Config', () => {
  beforeEach(() => {
    // reset singleton
    Config._resetInstance();
  });

  beforeAll(() => {
    enableFetchMocks();
  });

  beforeEach(() => {
    fetchMock.resetMocks();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('constructor', () => {
    it('should return same singleton instance', () => {
      const config1: Config = new Config();
      const config2: Config = new Config();
      expect(config1).toBe(config2);
      const config3: Config = new Config();
      expect(config1).toBe(config3);
    });

    it('should copy env', () => {
      const testVal = '123';
      process.env.TEST_VAL = testVal;
      const config: Config = new Config();
      expect(config.env.TEST_VAL).toEqual(testVal);
      delete process.env.TEST_VAL;
    });

    it('should log error if load URI attempt fails', async () => {
      try {
        const spyLoad = jest.spyOn(Config.prototype, 'loadURIEnvConfig').mockImplementation(jest.fn(() => {
          throw new Error('load fail');
        }));
        const spyLog = jest.spyOn(console, 'log').mockImplementation(jest.fn());

        process.env.DAMLOG_ENV_URI = 'http://something';
        let thrown: string | null = null;
        try {
          await Config.initConfig();
          await new Promise((r) => setTimeout(r, 1)); // force yield so fetch.then can run
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (err: any) {
          thrown = err.message;
        }
        expect(thrown).toEqual('load fail');
        expect(spyLoad).toHaveBeenCalled();
        expect(spyLog).toHaveBeenCalled();
      } finally {
        delete process.env.DAMLOG_ENV_URI;
      }
    });
  });

  describe('initConfig', () => {
    it('should update the config singleton', () => {
      const testVal = '123';
      process.env.TEST_VAL = testVal;
      const config: Config = new Config();
      expect(config.env.TEST_VAL).toEqual(testVal);
      delete process.env.TEST_VAL;

      const updatedVal = '54321';
      Config.initConfig({TEST_VAL: updatedVal});
      expect(config.env.TEST_VAL).toEqual(updatedVal);
    });
  });

  describe('DAMLOG_ENV', () => {
    it('should be default to test in test env', () => {
      const config: Config = new Config();
      expect(config.env.DAMLOG_ENV).toEqual('test');
    });

    it('should be default to DAMLOG_ENV if already in env', () => {
      try {
        process.env.DAMLOG_ENV = 'beta';
        const config: Config = new Config();
        expect(config.env.DAMLOG_ENV).toEqual('beta');
      } finally {
        delete process.env.DAMLOG_ENV;
      }
    });

    it('should be set to prod if NODE_ENV prod', () => {
      const oldEnv = process.env.NODE_ENV;
      try {
        process.env.NODE_ENV = 'production';
        const config: Config = new Config();
        expect(config.env.DAMLOG_ENV).toEqual('prod');
      } finally {
        process.env.NODE_ENV = oldEnv;
      }
    });

    it('should default to dev if NODE_ENV is not set', () => {
      const oldEnv = process.env.NODE_ENV;
      try {
        delete process.env.NODE_ENV;
        const config: Config = new Config();
        expect(config.env.DAMLOG_ENV).toEqual('dev');
      } finally {
        process.env.NODE_ENV = oldEnv;
      }
    });
  });

  describe('isProduction()', () => {
    it('should be true when DAMLOG_ENV is prod', () => {
      try {
        process.env.DAMLOG_ENV = 'prod';
        expect(Config.isProduction()).toBeTruthy();
      } finally {
        delete process.env.DAMLOG_ENV;
      }
    });

    it('should be false when DAMLOG_ENV is not prod', () => {
      try {
        process.env.DAMLOG_ENV = 'dev';
        expect(Config.isProduction()).toBeFalsy();
      } finally {
        delete process.env.DAMLOG_ENV;
      }
    });
  });

  describe('isDev()', () => {
    it('should be true when DAMLOG_ENV is dev', () => {
      try {
        process.env.DAMLOG_ENV = 'dev';
        expect(Config.isDev()).toBeTruthy();
      } finally {
        delete process.env.DAMLOG_ENV;
      }
    });

    it('should be false when DAMLOG_ENV is not dev', () => {
      try {
        process.env.DAMLOG_ENV = 'prod';
        expect(Config.isDev()).toBeFalsy();
      } finally {
        delete process.env.DAMLOG_ENV;
      }
    });
  });

  describe('loadURIEnvConfig', () => {
    const fixturesDir = path.join(__dirname, '..', '..', 'fixtures');
    const testConfigFile = `${fixturesDir}/testconf.json`;

    beforeAll(() => {
      enableFetchMocks();
    });

    afterAll(() => {
      disableFetchMocks();
      jest.restoreAllMocks();
    });

    it('should load from file', () => {
      try {
        process.env.DAMLOG_ENV_URI = testConfigFile;
        const config: Config = new Config();
        expect(config.env.resources?.staticHome).toEqual('http://localhost:3000/dev');
      } finally {
        delete process.env.DAMLOG_ENV_URI;
      }
    });

    it('should load from URL', async () => {
      try {
        process.env.DAMLOG_ENV_URI = 'http://localhost:3000/dev/test.conf';

        // mock fetch, return fixture file contents
        const payload = fs.readFileSync(testConfigFile, {encoding: 'utf-8'});
        const payloadObj = JSON.parse(payload);
        const reply = Config.forSend(Config.messages().extConfig, payloadObj); // always assume encrypted

        fetchMock.mockResponse(reply);

        const config: Config = new Config();
        await new Promise((r) => setTimeout(r, 1)); // force yield so fetch.then can run

        expect(config.env.resources?.staticTemplatesUrl).toEqual('http://localhost:3000/dev/templates');
      } finally {
        delete process.env.DAMLOG_ENV_URI;
      }
    });

    const loadObjectTreeTest = (overwrite: boolean) => {
      try {
        // first load from file
        process.env.DAMLOG_ENV_URI = testConfigFile;
        const config: Config = new Config();

        // load additional environment
        // - attempt to add new subtree
        // - attempt to replace existing nephew
        // - attempt to delete existing nephew
        // - attempt to add new nephew
        const newElement = {
          deeperElement: {
            deepLevel: 42,
          }
        };
        const newStaticExtConfigUrl = 'http://newurl';
        const addedResource = 'http://additionalurl';
        const addl = {
          logging: {
            newElement,
          },
          resources: {
            staticExtConfigUrl: newStaticExtConfigUrl,
            addedResource,
          }
        };
        config.loadEnvConfig(addl, overwrite);

        const expectedLoggingEnv = {
          logging: {
            level: 4,
            newElement: {
              deeperElement: {
                deepLevel: 42,
              }
            },
            subsystem1: {
              level: 5
            },
          }
        };
        const expectedResourcesEnv = {
          resources: {
            addedResource: 'http://additionalurl',
            staticExtConfigUrl: 'http://localhost:3000/dev/TODO',
            staticHome: 'http://localhost:3000/dev',
            staticTemplatesUrl: 'http://localhost:3000/dev/templates'
          }
        };
        const expectedOverwriteResourcesEnv = {
          resources: {
            addedResource: 'http://additionalurl',
            staticExtConfigUrl: 'http://newurl',
            staticHome: 'http://localhost:3000/dev',
            staticTemplatesUrl: 'http://localhost:3000/dev/templates'
          }
        };

        // check our additions that didn't disturb what was already there
        expect(config.env.logging).toEqual(expectedLoggingEnv.logging);

        // check our additions that didn't disturb what was already there, unless overwrite specified
        if (overwrite) {
          expect(config.env.resources).toEqual(expectedOverwriteResourcesEnv.resources);
        } else {
          expect(config.env.resources).toEqual(expectedResourcesEnv.resources);
        }
      } finally {
        delete process.env.DAMLOG_ENV_URI;
      }
    };

    it('should load from object tree without overwrite', () => {
      loadObjectTreeTest(false);
    });

    it('should load from object tree with overwrite', () => {
      loadObjectTreeTest(true);
    });
  });

  // really runs against damli-server
  // to run, remove the .skip
  describe.skip('damli server loadURIEnvConfig', () => {
    it('should load from URL', async () => {
      try {
        process.env.DAMLOG_ENV_URI = 'http://localhost:3000/dev/templates?config=extConfig';
        const config: Config = new Config();

        // force yield so fetch.then can run
        // needs a real delay since this it isn't mocked
        await new Promise((r) => setTimeout(r, 1000));

        expect(config.env.resources.parserUrl).toEqual('http://localhost:3000/dev/parser');
      } finally {
        delete process.env.DAMLOG_ENV_URI;
      }
    });

  });

  describe('setEnvConfig', () => {
    it('should replace env', () => {
      const config: Config = new Config();

      const oldEnv = {
        test: 'value',
        num: 42,
      };
      config.loadEnvConfig(oldEnv);
      expect(config.env.test).toEqual(oldEnv.test);
      expect(config.env.num).toEqual(oldEnv.num);

      const newEnv = {
        foo: 'hello',
        bar: 11,
      };
      config.setEnvConfig(newEnv);
      expect(config.env.test).toBeUndefined();
      expect(config.env.num).toBeUndefined();
      expect(config.env.foo).toEqual(newEnv.foo);
      expect(config.env.bar).toEqual(newEnv.bar);
    });
  });

  describe('messaging', () => {
    it('should export strings as messages', () => {
      expect(Object.keys(Config.messages()).length).toBeGreaterThan(1);
    });

    it('should prepare a string for sending and receive it', () => {
      const sending = 'This is a test message';
      const message = Config.messages().extConfig;
      const garbled = Config.forSend(message, sending);
      expect(garbled).not.toEqual(sending);
      const received = Config.forReceipt(message, garbled);
      expect(received).toEqual(sending);
    });

    it('should prepare an object for sending and receive it', () => {
      const sending = {
        start: 'this is the start',
        answer: 42,
        nested: {
          inner: 'inside object',
          again: {
            deeper: 'nested again',
          },
        },
      };
      const message = Config.messages().gatherTemplates;
      const garbled = Config.forSend(message, sending);
      expect(garbled).not.toEqual(sending);
      const received = Config.forReceipt(message, garbled);
      expect(received).toEqual(sending);
    });
  });

  describe('parser payload options', () => {
    it('should convert to flags string', () => {
      expect(Config.parserDataOptsStr({})).toEqual('');
      expect(Config.parserDataOptsStr({reduce: true, encode: true, compress: true})).toEqual('rec');
      expect(Config.parserDataOptsStr({reduce: false, encode: true, compress: true})).toEqual('ec');
      expect(Config.parserDataOptsStr({reduce: false, encode: true, compress: false})).toEqual('e');
      expect(Config.parserDataOptsStr({reduce: false, encode: false, compress: true})).toEqual('c');
    });

    it('should convert from flags string', () => {
      expect(Config.parserDataOpts('')).toEqual({reduce: false, encode: false, compress: false});
      expect(Config.parserDataOpts('rec')).toEqual({reduce: true, encode: true, compress: true});
      expect(Config.parserDataOpts('ec')).toEqual({reduce: false, encode: true, compress: true});
      expect(Config.parserDataOpts('e')).toEqual({reduce: false, encode: true, compress: false});
      expect(Config.parserDataOpts('c')).toEqual({reduce: false, encode: false, compress: true});
    });
  });
});
