import {Damlog} from '../../../libs/Damlog';
import {DamlogLevel} from '../../../types/Damlog';
import Config from '../../../libs/Config';
import fs from 'fs';

describe('DamlogFileStream', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let errorLogger: any = null;

  beforeAll(() => {
    Config.instance.loadEnvConfig({
      logging: {
        settings: {
          default: {
            file: {
              level: DamlogLevel.TRACE,
              settings: {
                default: {
                  filePath: '/tmp/damli-test.log',
                },
              },
            },
          },
        },
      },
    });
  });

  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation(jest.fn());
    jest.spyOn(console, 'debug').mockImplementation(jest.fn());
    errorLogger = jest.spyOn(console, 'error').mockImplementation(jest.fn());
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('DamlogFileStream', () => {
    it('should call file writer', () => {
      const log = Damlog.createLogger('myLogger');

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const fsMock = jest.spyOn(fs, 'appendFileSync').mockImplementation(jest.fn());
      log.debug('debug to a file');

      const expectedFilePath = '/tmp/damli-test.log';
      const expectedContents = expect.stringContaining('{"name":"myLogger","level":20,"levelName":"debug","msg":"debug to a file","time":');
      expect(fsMock).toHaveBeenCalledWith(expectedFilePath, expectedContents);
    });

    it('should console error but not throw error on missing file path', () => {
      const log = Damlog.createLogger('myLogger');

      const saveState = Config.instance.env.logging.settings.default.file.settings.default.filePath;
      try {
        Config.instance.env.logging.settings.default.file.settings.default.filePath = undefined;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const fsMock = jest.spyOn(fs, 'appendFileSync').mockImplementation(jest.fn());
        log.debug('debug to a file but no file');

        expect(errorLogger).toHaveBeenCalledTimes(1);
        expect(fsMock).not.toHaveBeenCalled();
      } finally {
        Config.instance.env.logging.settings.default.file.settings.default.filePath = saveState;
      }
    });

    it('should console error but not allow thrown error', () => {
      const log = Damlog.createLogger('myLogger');

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const fsMock = jest.spyOn(fs, 'appendFileSync').mockImplementation(() => {
        throw new Error('test error should not make it out');
      });
      expect(() => {
        log.debug('debug to a file but no file');
      }).not.toThrow();

      expect(fsMock).toHaveBeenCalled();
      expect(errorLogger).toHaveBeenCalledTimes(1);
    });
  });
});
