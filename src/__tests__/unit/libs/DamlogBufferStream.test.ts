import {Damlog} from '../../../libs/Damlog';
import {DamlogLevel} from '../../../types/Damlog';
import Config from '../../../libs/Config';

describe('DamlogBufferStream', () => {
  beforeAll(() => {
    Config.instance.loadEnvConfig({
      logging: {
        settings: {
          default: {
            buffer: {
              level: DamlogLevel.TRACE,
              settings: {
                default: {
                  entryLimit: 3,
                },
              },
            },
          },
        },
      },
    });
  });

  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation(jest.fn());
    jest.spyOn(console, 'debug').mockImplementation(jest.fn());
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('DamlogBufferStream', () => {
    it('should append to memory buffer', () => {
      const log = Damlog.createLogger('myLogger');

      log.debug('first debug entry');
      log.warn('second warn entry');
      log.error('third error entry', {arg: 'additional', num: 42});

      expect(log.buffer.length).toEqual(3);

      const expectation = [
        expect.objectContaining(
          {
            level: 20,
            levelName: 'debug',
            msg: 'first debug entry',
            name: 'myLogger',
            time: expect.stringContaining('T'),
            v: 1,
          }
        ),
        expect.objectContaining(
          {
            level: 40,
            levelName: 'warn',
            msg: 'second warn entry',
            name: 'myLogger',
            time: expect.stringContaining('T'),
            v: 1,
          }
        ),
        expect.objectContaining(
          {
            level: 50,
            levelName: 'error',
            msg: 'third error entry',
            name: 'myLogger',
            time: expect.stringContaining('T'),
            v: 1,
            data: {
              arg: 'additional',
              num: 42,
            },
          }
        ),
        expect.objectContaining(
          {
            level: 50,
            levelName: 'error',
            msg: 'fourth is over the limit',
            name: 'myLogger',
            time: expect.stringContaining('T'),
            v: 1,
          }
        ),
      ];

      [0, 1, 2].forEach((i) => {
        const response = JSON.parse(log.buffer[i]);
        expect(response).toEqual(expectation[i]);
      });

      // another one should exceed the limit and cause the first message to be dropped
      log.error('fourth is over the limit');
      expect(log.buffer.length).toEqual(3);
      expectation.shift();
      expect(log.buffer.map(e => JSON.parse(e))).toEqual(expectation);

      // should clear it
      log.reset();
      expect(log.buffer.length).toEqual(0);
    });
  });
});
