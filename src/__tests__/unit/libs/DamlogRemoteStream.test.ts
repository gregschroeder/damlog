import {DamlogLevel} from '../../../types/Damlog';
import Config from '../../../libs/Config';
import {disableFetchMocks, enableFetchMocks, MockResponseInit} from 'jest-fetch-mock';
import {Damlog} from '../../../libs/Damlog';
import {DamHttpClientRequest} from '../../../libs/DamHttpClient';

describe('DamlogRemoteStream', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let errorLogger: any = null;
  const name = 'SubSystem';

  beforeAll(() => {
    Config.instance.loadEnvConfig({
      logging: {
        settings: {
          default: {
            remote: {
              level: DamlogLevel.TRACE,
              settings: {
                default: {
                  loggerUrl: 'http://localhost:3000/dev/logRelay',
                  filePath: '/tmp/damli-test.log',
                },
              },
            },
          },
        },
      },
    });

    enableFetchMocks();
  });

  afterAll(() => {
    disableFetchMocks();
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation(jest.fn());
    jest.spyOn(console, 'debug').mockImplementation(jest.fn());
    errorLogger = jest.spyOn(console, 'error').mockImplementation(jest.fn());
  });

  afterEach(() => {
    fetchMock.resetMocks();
    jest.restoreAllMocks();
  });

  describe('DamlogRemoteStream', () => {
    it('should send log event to remote URL', async () => {
      // mock fetch
      const payload = {
        message: `logged ${name}`,
      };
      let mockRequest: DamHttpClientRequest | null = null;
      fetchMock.mockResponse((request: DamHttpClientRequest): Promise<MockResponseInit> => {
        mockRequest = request;
        return Promise.resolve({
          body: JSON.stringify(payload),
          init: {
            status: 200,
          }
        });
      });

      const log = Damlog.createLogger(name);

      // call logger
      const logMessage = 'test log.info() call';
      const dataObj = {
        hey: 'there',
      };
      log.info(logMessage, dataObj);

      // assert log command sent
      const logCommand = {
        name,
        level: 'INFO',
        msg: 'test log.info() call',
        filePath: '/tmp/damli-test.log',
        dataStr: JSON.stringify({data: dataObj}),
      };

      expect(mockRequest).not.toBeNull();
      if (mockRequest) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const body = await (<any>mockRequest).body;
        const dataStr = JSON.parse(`${body}`);
        const data = JSON.parse(dataStr);
        expect(data).toEqual(expect.objectContaining(logCommand));
      }
    });

    it('should console error but not throw error on missing file path', () => {
      const log = Damlog.createLogger(name);

      const saveState = Config.instance.env.logging.settings.default.remote.settings.default.loggerUrl;
      const mock = fetchMock.mockResponse('not called');
      try {
        Config.instance.env.logging.settings.default.remote.settings.default.loggerUrl = undefined;
        log.debug('debug to a file but no file');

        expect(errorLogger).toHaveBeenCalledTimes(1);
        expect(mock).toHaveBeenCalledTimes(0);
      } finally {
        Config.instance.env.logging.settings.default.remote.settings.default.loggerUrl = saveState;
      }
    });

    it('should console rejection error but not allow thrown error', async () => {
      const log = Damlog.createLogger(name);
      fetchMock.mockResponse((): Promise<MockResponseInit> => {
        return Promise.resolve({
          body: JSON.stringify({success: false}),
          init: {
            status: 200,
          }
        });
      });
      expect(() => {
        log.debug('debug to a file but no file');
      }).not.toThrow();

      // force yield so logging promises can run, needs a real delay
      await new Promise((r) => setTimeout(r, 1000));

      expect(errorLogger).toHaveBeenCalledTimes(1);
    });
  });
});
