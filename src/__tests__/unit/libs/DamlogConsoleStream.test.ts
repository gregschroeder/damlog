import {Damlog} from '../../../libs/Damlog';
import {DAMLOG_DEFAULT_FORMATTING, DamlogConsoleStream} from '../../../libs/DamlogConsoleStream';
import {DamlogColor, DamlogFormatting, DamlogFormattingDirective, DamlogLevel} from '../../../types/Damlog';
import Config from '../../../libs/Config';

// to validate colors/formatting, set to true and run
const manual = false;

describe('DamlogConsoleStream', () => {
  beforeAll(() => {
    Config.instance.loadEnvConfig({
      logging: {
        settings: {
          default: {
            console: {
              level: DamlogLevel.TRACE,
            },
          }
        }
      },
    });
  });

  beforeEach(() => {
    if (!manual) {
      jest.spyOn(console, 'log').mockImplementation(jest.fn());
      jest.spyOn(console, 'debug').mockImplementation(jest.fn());
      jest.spyOn(console, 'error').mockImplementation(jest.fn());
      jest.spyOn(console, 'trace').mockImplementation(jest.fn());
      jest.spyOn(console, 'info').mockImplementation(jest.fn());
    }
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('DamlogConsoleStream', () => {
    it('should display styles for manual checking when flag set', () => {
      const log = Damlog.createLogger('myLogger');

      expect(log.name).toEqual('myLogger');
      expect(log.level).toEqual(DamlogLevel.TRACE);

      log.trace('hi trace rrr');
      log.debug('hi debug rrr');
      log.warn('hi warn rrr');
      log.info('hi info rrr');
      log.error('hi error rrr');
      log.fatal('hi fatal rrr');

      log.trace('hi trace booboo', {bar: 1});
      log.debug('hi debug booboo', {bar: 1});
      log.warn('hi warn booboo', {bar: 1});
      log.info('hi info booboo', {bar: 1});
      log.error('hi error booboo', {bar: 1});
      log.fatal('hi fatal booboo', {bar: 1});

      log.trace({objfirst: {nest: 'hello'}}, 'hi trace objfirst');
      log.debug({objfirst: 1}, 'hi debug asdf');
      log.warn({objfirst: 1}, 'hi warn asdf');
      log.info({objfirst: 1}, 'hi info asdf');
      log.error({objfirst: 1}, 'hi error asdf');
      log.fatal({objfirst: 1}, 'hi fatal asdf');
    });

    it('should pad zeroes correctly', () => {
      const padZeroes = DamlogConsoleStream['padZeroes'];
      expect(padZeroes(7, 2)).toEqual('07');
      expect(padZeroes(7, 3)).toEqual('007');
    });

    it('should formatItemChalk correctly', () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const formatting: DamlogFormatting = DAMLOG_DEFAULT_FORMATTING['dark'];
      const dcs: DamlogConsoleStream = new DamlogConsoleStream();
      const logDirective: string[] = [];

      dcs['formatItemChalk']('[12:12:12.012]', <DamlogFormattingDirective>formatting?.default?.timestamp, logDirective);
      dcs['formatItemChalk']('INFO', <DamlogFormattingDirective>formatting?.default?.level, logDirective);
      dcs['formatItemChalk']('mySubsystem', <DamlogFormattingDirective>formatting?.default?.name, logDirective);
      dcs['formatItemChalk']('hello', <DamlogFormattingDirective>formatting?.default?.message, logDirective);

      // and reference some colors with names and not hex values, just for coverage completion
      dcs['formatItemChalk']('extra item', {color: DamlogColor.black, backgroundColor: DamlogColor.black}, logDirective);

      // to debug, uncomment and use hexdump -C /tmp/foobar.txt to see actual encoding
      // fs.writeFileSync('/tmp/foobar.txt', logDirective[3]);

      // NB: this test really needs human visual verification whenever anything changes
      // running in iTerm vs in IDEA gives different results.  use presence of LC_TERMINAL to detect former.
      if (process.env.LC_TERMINAL) {
        expect(logDirective).toEqual([
          '\u001b[3m\u001b[38;2;136;136;136m[12:12:12.012]\u001b[39m\u001b[23m',
          '\u001b[38;2;0;188;227mINFO\u001b[39m',
          '\u001b[38;2;46;103;248mmySubsystem\u001b[39m',
          '\u001b[38;2;255;255;237mhello\u001b[39m',
          '\u001b[40m\u001b[30mextra item\u001b[39m\u001b[49m'
        ]);
      } else {
        expect(logDirective).toEqual([
          '\u001b[3m\u001b[37m[12:12:12.012]\u001b[39m\u001b[23m',
          '\u001b[96mINFO\u001b[39m',
          '\u001b[94mmySubsystem\u001b[39m',
          '\u001b[97mhello\u001b[39m',
          '\u001b[40m\u001b[30mextra item\u001b[39m\u001b[49m'
        ]);
      }
    });

    it('should formatItemCSS correctly', () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const formatting: DamlogFormatting = DAMLOG_DEFAULT_FORMATTING['dark'];
      const dcs: DamlogConsoleStream = new DamlogConsoleStream();
      const logDirective: string[] = [];
      const logArgs: string[] = [];

      dcs['formatItemCSS']('[12:12:12.012]', <DamlogFormattingDirective>formatting?.default?.timestamp, logDirective, logArgs);
      dcs['formatItemCSS']('INFO', <DamlogFormattingDirective>formatting?.default?.level, logDirective, logArgs);
      dcs['formatItemCSS']('mySubsystem', <DamlogFormattingDirective>formatting?.default?.name, logDirective, logArgs);
      dcs['formatItemCSS']('hello', <DamlogFormattingDirective>formatting?.default?.message, logDirective, logArgs);

      // and reference some colors with names and not hex values, just for coverage completion
      dcs['formatItemCSS']('extra item', {color: DamlogColor.black, backgroundColor: DamlogColor.black, weight: 'bold'}, logDirective, logArgs);

      // and with no CSS at all, doesn't bother with subst
      dcs['formatItemCSS']('another extra item', {}, logDirective, logArgs);

      // NB: this test really needs human visual verification whenever anything changes
      expect(logDirective).toEqual([
        '%c',
        '%s',
        '%c',
        '%s',
        '%c',
        '%s',
        '%c',
        '%s',
        '%c',
        '%s',
        'another extra item'
      ]);
      expect(logArgs).toEqual([
        'color: #888888; font-style: italic',
        '[12:12:12.012]',
        'color: #00BCE3',
        'INFO',
        'color: #2E67F8',
        'mySubsystem',
        'color: #FFFFED',
        'hello',
        'color: black; background-color: black; font-weight: bold',
        'extra item'
      ]);
    });

    it('should use chalk when not in browser', () => {
      const dcs: DamlogConsoleStream = new DamlogConsoleStream();

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const cssFunc = jest.spyOn(DamlogConsoleStream.prototype as any, 'formatItemCSS').mockImplementation(jest.fn());
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const chalkFunc = jest.spyOn(DamlogConsoleStream.prototype as any, 'formatItemChalk').mockImplementation(jest.fn());

      const logDirective: string[] = [];
      const logArgs: string[] = [];

      const saveState = dcs['_inBrowser'];
      try {
        dcs['_inBrowser'] = false;
        dcs['formatItem']('an item', {}, logDirective, logArgs);
        expect(cssFunc).not.toHaveBeenCalled();
        expect(chalkFunc).toHaveBeenCalledTimes(1);
      } finally {
        dcs['_inBrowser'] = saveState;
      }
    });

    it('should use CSS in browser', () => {
      const dcs: DamlogConsoleStream = new DamlogConsoleStream();

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const cssFunc = jest.spyOn(DamlogConsoleStream.prototype as any, 'formatItemCSS').mockImplementation(jest.fn());
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const chalkFunc = jest.spyOn(DamlogConsoleStream.prototype as any, 'formatItemChalk').mockImplementation(jest.fn());

      const logDirective: string[] = [];
      const logArgs: string[] = [];

      const saveState = dcs['_inBrowser'];
      try {
        dcs['_inBrowser'] = true;
        dcs['formatItem']('an item', {}, logDirective, logArgs);
        expect(cssFunc).toHaveBeenCalledTimes(1);
        expect(chalkFunc).not.toHaveBeenCalled();
      } finally {
        dcs['_inBrowser'] = saveState;
      }
    });
  });
});
