export const DamlogColor = {
  black: 'black',
  gray: '#888888',
  red: '#FF033E',
  green: '#009A17',
  yellow: '#FEDD00',
  blue: '#2E67F8',
  magenta: '#D53F77',
  cyan: '#00BCE3',
  white: '#FFFFED',
  orange: '#FF8200'
} as const;
export type DamlogColor = typeof DamlogColor[keyof typeof DamlogColor];

export type DamlogDarkMode = 'light' | 'dark';

export interface DamlogFormattingDirective {
  color?: DamlogColor,
  backgroundColor?: DamlogColor,
  weight?: 'normal' | 'bold',
  modifier?: 'normal' | 'italic',
}

export interface DamlogItemFormatting {
  message?: DamlogFormattingDirective;   // default entire output line
  timestamp?: DamlogFormattingDirective, // timestamp
  level?: DamlogFormattingDirective,     // level name, eg, INFO
  name?: DamlogFormattingDirective,      // logger name, eg, component, subsystem
}

export interface DamlogFormatting {
  default?: DamlogItemFormatting,
  TRACE?: DamlogItemFormatting,
  DEBUG?: DamlogItemFormatting,
  INFO?: DamlogItemFormatting,
  WARN?: DamlogItemFormatting,
  ERROR?: DamlogItemFormatting,
  FATAL?: DamlogItemFormatting,
}

// NB: keep values in sync with Bunyan for interoperability
export enum DamlogLevel {
  TRACE = 10,
  DEBUG = 20,
  INFO = 30,
  WARN = 40,
  ERROR = 50,
  FATAL = 60,
  OFF = 100,
}

export interface DamlogRecord {
  name: string,
  childName: string,
  level: DamlogLevel,
  time: Date,
  msg: string | object,
}

export type DamlogStreamType = 'console' | 'remote' | 'file' | 'buffer';

export interface DamlogStream {
  write(record: object): void;
}

export interface DamlogConsoleStreamSettings {
  darkMode: DamlogDarkMode,
}

export interface DamlogRemoteStreamSettings {
  loggerUrl: string,
  filePath?: string,
}

export interface DamlogFileStreamSettings {
  filePath: string,
}

export interface DamlogBufferStreamSettings {
  entryLimit: number,
}

export interface DamlogLevelSettings<T> {
  default?: T,
  TRACE?: T,
  DEBUG?: T,
  INFO?: T,
  WARN?: T,
  ERROR?: T,
  FATAL?: T,
}

// configuration for the named loggers and their use of the streams
// allow different settings for different levels,
// eg, so that errors can head to a different stream than info
export interface DamlogSettings {
  console?: {
    level: DamlogLevel,
    settings?: DamlogLevelSettings<DamlogConsoleStreamSettings>,
  },
  remote?: {
    level: DamlogLevel,
    settings?: DamlogLevelSettings<DamlogRemoteStreamSettings>,
  },
  file?: {
    level: DamlogLevel,
    settings?: DamlogLevelSettings<DamlogFileStreamSettings>,
  },
  buffer?: {
    level: DamlogLevel,
    settings?: DamlogLevelSettings<DamlogBufferStreamSettings>,
  },
}

export interface DamlogNamedSettings {
  [key: string]: DamlogSettings,
}

export interface DamlogLogger {
  get name(): string;
  get level(): DamlogLevel;
  get buffer(): string[];
  reset(): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trace(msg: string | object, data?: any): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  debug(msg: string | object, data?: any): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  warn(msg: string | object, data?: any): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  info(msg: string | object, data?: any): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error(msg: string | object, data?: any): void;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fatal(msg: string | object, data?: any): void;

  alert(msg: string | object): void;
}
